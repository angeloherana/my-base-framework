﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using AutoMapper;
using Base.Server.API.DTO;
using Base.Server.API.ServiceModel.AppKey;
using Base.Server.API.ServiceModel.Customer;
using Base.Server.API.ServiceModel.Group;
using Base.Server.API.ServiceModel.User;
using Base.Server.API.ServiceModel.UserGroup;
using Base.Server.Models;
using MenuItem = Base.Server.Models.Menu;

namespace Base.Server.API.Tasks
{
    public class AutoMapperConfigTask : ITask
    {
        public void Execute()
        {
            //API
            Mapper.CreateMap<CreateAppKey, AppKeyDTO>();
            Mapper.CreateMap<AppKeyDTO, CreateAppKey>();
            Mapper.CreateMap<AppKeyDTO, AppKey>();
            Mapper.CreateMap<AppKey, AppKeyDTO>();

            //User
            Mapper.CreateMap<CreateUser, UserDTO>();
            Mapper.CreateMap<UserDTO, CreateUser>();
            Mapper.CreateMap<UpdateUser, UserDTO>();
            Mapper.CreateMap<UserDTO, UpdateUser>();
            Mapper.CreateMap<LoginUserResponse, UserDTO>();
            Mapper.CreateMap<UserDTO, LoginUserResponse>();
            Mapper.CreateMap<UserDTO, User>();
            Mapper.CreateMap<User, UserDTO>();

            //Group
            Mapper.CreateMap<CreateGroup, GroupDTO>();
            Mapper.CreateMap<GroupDTO, CreateGroup>();
            Mapper.CreateMap<UpdateGroup, GroupDTO>();
            Mapper.CreateMap<GroupDTO, UpdateGroup>();
            Mapper.CreateMap<GroupDTO, Group>();
            Mapper.CreateMap<Group, GroupDTO>();

            //UserGroup
            Mapper.CreateMap<CreateUserGroup, UserGroupDTO>();
            Mapper.CreateMap<UserGroupDTO, CreateUserGroup>();
            Mapper.CreateMap<UserGroupDTO, UserGroup>();
            Mapper.CreateMap<UserGroup, UserGroupDTO>();

            //Customer
            Mapper.CreateMap<CreateCustomer, CustomerDTO>();
            Mapper.CreateMap<CustomerDTO, CreateCustomer>();
            Mapper.CreateMap<UpdateCustomer, CustomerDTO>();
            Mapper.CreateMap<CustomerDTO, UpdateCustomer>();
            Mapper.CreateMap<CustomerDTO, Customer>();
            Mapper.CreateMap<Customer, CustomerDTO>();

            //Menu
            Mapper.CreateMap<MenuItem, MenuDTO>();
            Mapper.CreateMap<MenuDTO, MenuItem>();

            //MenuGroup
            Mapper.CreateMap<MenuGroup, MenuGroupDTO>();
            Mapper.CreateMap<MenuGroupDTO, MenuGroup>();
        }
    }
}