﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using Funq;
using ServiceStack;
using ServiceStack.Common.Web;
using ServiceStack.ServiceInterface;
using ServiceStack.ServiceInterface.Auth;
using ServiceStack.Text;
using ServiceStack.WebHost.Endpoints;
using Base.Server.API.Services;
using Base.Server.API.Services.Interfaces;
using Base.Server.API.Tasks;
using Base.Server.Infrastructure.Services;
using Base.Server.Infrastructure.Services.Interfaces;
using Base.Server.Repositories.Factory;
using Base.Server.Repositories.Providers;
using Base.Server.Repositories.UnitOfWork;
using Base.Server.Services;
using Base.Server.Services.Interfaces;

namespace Base.Server.API
{
    public class Global : HttpApplication
    {
        public class BaseAppHost : AppHostBase
        {
            //Tell Service Stack the name of your application and where to find your web services
            public BaseAppHost() : base("Base Server Web API", typeof(AppKeyAPIService).Assembly) { }

            public override void Configure(Container container)
            {

                //register any dependencies your services use, e.g:
                //container.Register<IRepositoryProvider>(new RepositoryProvider(new RepositoryFactories()));
                container.Register(new RepositoryFactories());
                container.Register<IRepositoryProvider>(c => new RepositoryProvider(container.TryResolve<RepositoryFactories>())).ReusedWithin(ReuseScope.None);
                container.Register<IBaseUow>(c => new BaseUow(container.TryResolve<IRepositoryProvider>())).ReusedWithin(ReuseScope.None);

                container.Register<IAppKeyService>(c => new AppKeyService(container.TryResolve<IBaseUow>())).ReusedWithin(ReuseScope.None);
                container.Register<IUserService>(c => new UserService(container.TryResolve<IBaseUow>())).ReusedWithin(ReuseScope.None);
                container.Register<IGroupService>(c => new GroupService(container.TryResolve<IBaseUow>())).ReusedWithin(ReuseScope.None);
                container.Register<IUserGroupService>(c => new UserGroupService(container.TryResolve<IBaseUow>())).ReusedWithin(ReuseScope.None);
                container.Register<ICustomerService>(c => new CustomerService(container.TryResolve<IBaseUow>())).ReusedWithin(ReuseScope.None);
                container.Register<IMenuService>(c => new MenuService(container.TryResolve<IBaseUow>())).ReusedWithin(ReuseScope.None);
                container.Register<IMenuGroupService>(c => new MenuGroupService(container.TryResolve<IBaseUow>())).ReusedWithin(ReuseScope.None);

                container.Register<ITask>(t => new CompositeTask(new AutoMapperConfigTask()));

                container.Resolve<ITask>().Execute();

                Plugins.Add(new AuthFeature(
                    () => new AuthUserSession(),
                    new IAuthProvider[]
                    {
                        new BasicAuthProvider(),  
                    }
                ));


                //Check if API key is valid
                RequestFilters.Add((req, res, dto) =>
                {
                    var apiKey = req.Headers["X-ApiKey"];
                    var isAuthorized = container.Resolve<IAppKeyService>().CheckIfApiKeyIsValid(apiKey);
                    if (apiKey == null || !isAuthorized)
                    {
                        throw HttpError.Unauthorized("Unauthorized Api Key");
                    }
                });
            }
        }

        protected void Application_Start(object sender, EventArgs e)
        {
            new BaseAppHost().Init();
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}