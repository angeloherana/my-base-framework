﻿using System;
using System.Collections.Generic;
using Base.Server.Infrastructure.Services;
using Base.Server.Services.Interfaces;
using Base.Server.Models;
using Base.Server.Repositories.Factory;
using Base.Server.Repositories.Interfaces;
using Base.Server.Repositories.Providers;
using Base.Server.Repositories.UnitOfWork;
using NUnit.Framework;
using Base.Server.Services.UnitTests.FakeObjects;

namespace Base.Server.Services.UnitTests
{
    [TestFixture]
    public class AppKeyServiceTests
    {

        [Test]
        public void GetAllAppKey_Test()
        {
            IAppKeyService appKeyService = new AppKeyService(new BaseUow(new RepositoryProvider(new RepositoryFactories())));

            var appKeys = appKeyService.GetAllAppKey();
            Assert.IsNotEmpty(appKeys);
        }

        [Test]
        public void CreateAppKey_Test()
        {
            IAppKeyService appKeyService = new AppKeyService(new BaseUow(new RepositoryProvider(new RepositoryFactories())));
            var appKeyBuilder = new AppKeyBuilder("API Test 1","apitest1");
            var appKey = appKeyBuilder.Build();

            var addedAppKey = appKeyService.CreateAppKey(appKey);
            Assert.IsNotNull(addedAppKey);
        }

        [Test]
        public void CheckIfApiKeyIsValid_Test()
        {            IAppKeyService appKeyService = new AppKeyService(new BaseUow(new RepositoryProvider(new RepositoryFactories())));


            var isValid = appKeyService.CheckIfApiKeyIsValid("apitest1");

            Assert.IsTrue(isValid);
        }
    }


}
