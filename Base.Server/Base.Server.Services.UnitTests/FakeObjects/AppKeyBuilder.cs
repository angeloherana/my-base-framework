﻿using System;
using Base.Server.Infrastructure.Services;
using Base.Server.Infrastructure.Services.Interfaces;
using Base.Server.Models;

namespace Base.Server.Services.UnitTests.FakeObjects
{
    internal class AppKeyBuilder
    {
        private readonly AppKey _appKey;
        public AppKeyBuilder(string name, string apiKey)
        {
            _appKey = new AppKey
            {
                Name = name,
                ApiKey = apiKey
            };
        }

        public AppKey Build()
        {
            return _appKey;
        }

    }
}
