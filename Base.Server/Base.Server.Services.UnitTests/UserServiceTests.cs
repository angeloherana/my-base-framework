﻿using System;
using System.Collections.Generic;
using System.Linq;
using Base.Server.Infrastructure.Services;
using Base.Server.Services.Interfaces;
using Base.Server.Models;
using Base.Server.Repositories.Factory;
using Base.Server.Repositories.Interfaces;
using Base.Server.Repositories.Providers;
using Base.Server.Repositories.UnitOfWork;
using NUnit.Framework;
using Base.Server.Services.UnitTests.FakeObjects;

namespace Base.Server.Services.UnitTests
{
    [TestFixture]
    public class UserServiceTests
    {
        readonly IUserService _userService = new UserService(new BaseUow(new RepositoryProvider(new RepositoryFactories())));

        [Test]
        public void CreateUser_Test()
        {

            var user = new User
            {
                FirstName = "Customer",
                LastName = "Admin",
                UserName = "CustomerAdmin",
                Password = "password",
                CreatedBy = "System",
                IsActive = true
            };

            var newUser = _userService.CreateUser(user);
            Assert.IsNotNull(newUser);
        }

        [Test]
        public void GetUserByUserId()
        {
            var user = _userService.GetUserByUserId(3);

            Assert.IsNotNull(user);
        }

        [Test]
        public void UpdateUser_Test()
        {
            var user = _userService.GetUserByUserId(3);
            var lastUpdatedDate = user.UpdatedDate;

            user.UpdatedBy = "System";

            var existingUser = _userService.UpdateUser(user);

            Assert.AreNotEqual(lastUpdatedDate, existingUser.UpdatedDate);
        }

        [Test]
        public void DeleteUser_Test()
        {

            var isDeleted = _userService.DeleteUser(3, "System");

            Assert.IsTrue(isDeleted);

        }
        [Test]
        public void GetActiveUsers_Test()
        {
            var users = _userService.GetActiveUsers();

            Assert.IsTrue(users.Any());
        }
    }


}
