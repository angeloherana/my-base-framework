﻿using System;
using System.Collections.Generic;
using System.Linq;
using Base.Server.Infrastructure.Services;
using Base.Server.Services.Interfaces;
using Base.Server.Models;
using Base.Server.Repositories.Factory;
using Base.Server.Repositories.Interfaces;
using Base.Server.Repositories.Providers;
using Base.Server.Repositories.UnitOfWork;
using NUnit.Framework;
using Base.Server.Services.UnitTests.FakeObjects;

namespace Base.Server.Services.UnitTests
{
    [TestFixture]
    public class GroupServiceTests
    {
        readonly IGroupService _groupService = new GroupService(new BaseUow(new RepositoryProvider(new RepositoryFactories())));

        [Test]
        public void CreateGroup_Test()
        {
            var group = new Group
            {
                Name = "Admin",
                Description = "Admin",
                CreatedBy = "System", 
                IsActive = true
            };

            var newGroup = _groupService.CreateGroup(group);
            Assert.IsNotNull(newGroup);
        }

        [Test]
        public void GetGroupByGroupId()
        {
            var group = _groupService.GetGroupByGroupId(2);

            Assert.IsNotNull(group);
        }

        [Test]
        public void UpdateGroup_Test()
        {
            var group = _groupService.GetGroupByGroupId(2);
            var lastUpdatedDate = group.UpdatedDate;

            group.UpdatedBy = "System";

            var existingGroup = _groupService.UpdateGroup(group);

            Assert.AreNotEqual(lastUpdatedDate, existingGroup.UpdatedDate);
        }

        [Test]
        public void DeleteGroup_Test()
        {

            var isDeleted = _groupService.DeleteGroup(2, "System");

            Assert.IsTrue(isDeleted);

        }
        [Test]
        public void GetActiveGroups_Test()
        {
            var users = _groupService.GetActiveGroups();

            Assert.IsTrue(users.Any());
        }
    }


}
