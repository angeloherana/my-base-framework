﻿using System;
using System.Collections.Generic;
using System.Linq;
using Base.Server.Infrastructure.Services;
using Base.Server.Services.Interfaces;
using Base.Server.Models;
using Base.Server.Repositories.Factory;
using Base.Server.Repositories.Interfaces;
using Base.Server.Repositories.Providers;
using Base.Server.Repositories.UnitOfWork;
using NUnit.Framework;
using Base.Server.Services.UnitTests.FakeObjects;

namespace Base.Server.Services.UnitTests
{
    [TestFixture]
    public class CustomerServiceTests
    {
        readonly ICustomerService _customerService = new CustomerService(new BaseUow(new RepositoryProvider(new RepositoryFactories())));

        [Test]
        public void CreateCustomer_Test()
        {

            var customer = new Customer
            {
                FirstName = "Inday",
                LastName = "Batuta",
                CreatedBy = "System",
                IsActive = true
            };

            var newCustomer = _customerService.CreateCustomer(customer);
            Assert.IsNotNull(newCustomer);
        }

        [Test]
        public void GetCustomerByCustomerId()
        {
            var customer = _customerService.GetCustomerByCustomerId(2);

            Assert.IsNotNull(customer);
        }

        [Test]
        public void UpdateCustomer_Test()
        {
            var customer = _customerService.GetCustomerByCustomerId(2);
            var lastUpdatedDate = customer.UpdatedDate;

            customer.UpdatedBy = "System";

            var existingCustomer = _customerService.UpdateCustomer(customer);

            Assert.AreNotEqual(lastUpdatedDate, existingCustomer.UpdatedDate);
        }

        [Test]
        public void DeleteCustomer_Test()
        {

            var isDeleted = _customerService.DeleteCustomer(2, "System");

            Assert.IsTrue(isDeleted);

        }
        [Test]
        public void GetActiveCustomers_Test()
        {
            var users = _customerService.GetActiveCustomers();

            Assert.IsTrue(users.Any());
        }
    }


}
