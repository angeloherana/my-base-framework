﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Base.Server.Repositories.Factory;
using Base.Server.Repositories.Providers;
using Base.Server.Repositories.UnitOfWork;
using Base.Server.Services.Interfaces;

namespace Base.Server.Services.UnitTests
{
    [TestFixture]
    public class MenuServiceTests
    {
        readonly IMenuService _menuService = new MenuService(new BaseUow(new RepositoryProvider(new RepositoryFactories())));
        readonly IUserService _userService = new UserService(new BaseUow(new RepositoryProvider(new RepositoryFactories())));

        [Test]
        public void GetParentMenu_Test()
        {
            var user = _userService.GetUserByUserId(3);
            var parentMenus = _menuService.GetParentMenuByGroup(user.UserGroups.ToList());

            foreach (var parentMenu in parentMenus)
            {
                Assert.IsNull(parentMenu.ParentMenuId);
            }
        }
        [Test]
        public void GetChildMenu_Test()
        {
            var user = _userService.GetUserByUserId(3);
            var childMenus = _menuService.GetChildMenuByGroup(user.UserGroups.ToList(),1);

            foreach (var childMenu in childMenus)
            {
                Assert.IsNotNull(childMenu.ParentMenuId);
            }
        }
    }
}
