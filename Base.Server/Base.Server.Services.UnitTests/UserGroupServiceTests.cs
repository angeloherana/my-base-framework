﻿using System;
using System.Collections.Generic;
using System.Linq;
using Base.Server.Infrastructure.Services;
using Base.Server.Services.Interfaces;
using Base.Server.Models;
using Base.Server.Repositories.Factory;
using Base.Server.Repositories.Interfaces;
using Base.Server.Repositories.Providers;
using Base.Server.Repositories.UnitOfWork;
using NUnit.Framework;
using Base.Server.Services.UnitTests.FakeObjects;

namespace Base.Server.Services.UnitTests
{
    [TestFixture]
    public class UserGroupServiceTests
    {
        readonly IUserGroupService _userGroupService = new UserGroupService(new BaseUow(new RepositoryProvider(new RepositoryFactories())));

        [Test]
        public void CreateUserGroup_Test()
        {

            var userGroup = new UserGroup
            {
                UserId = 4,
                GroupId = 3,
                CreatedBy = "System",
            };

            var newUserGroup = _userGroupService.CreateUserGroup(userGroup);
            Assert.IsNotNull(newUserGroup);
        }

        [Test]
        public void DeleteUserGroup_Test()
        {

            var isDeleted = _userGroupService.DeleteUserGroup(1);

            Assert.IsTrue(isDeleted);

        }
        [Test]
        public void GetAllUserGroups_Test()
        {
            var userGroups = _userGroupService.GetAllUserGroups();

            Assert.IsTrue(userGroups.Any());
        }
    }


}
