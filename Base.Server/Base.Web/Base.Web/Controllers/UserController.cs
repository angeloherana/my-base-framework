﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Base.Web.ApplicationServices;
using Base.Web.ApplicationServices.Interfaces;
using Base.Web.Controllers.Base;
using Base.Web.Controllers.CustomActionResult;

namespace Base.Web.Controllers
{
    public class UserController : BaseController
    {
        readonly IUserAppService _userAppService = new UserAppService();
        //
        // GET: /User/
        public ActionResult Index()
        {
            return View();
        }

        #region partialview
        public ActionResult List()
        {
            return PartialView("_List");
        }

        public ActionResult Create()
        {
            return PartialView("_Create");
        }

        #endregion

        #region methods

        [HttpGet]
        public JsonNetResult GetUsers()
        {
            var users = _userAppService.GetActiveUsers().ToList();

            return new JsonNetResult()
            {
                Data = new { Users = users, Count = users.Count() }
            };
        }

        #endregion
    }
}