﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using Base.Web.ApplicationServices;
using Base.Web.ApplicationServices.Interfaces;
using Base.Web.Controllers.Base;
using Base.Web.Models;
using ServiceStack.ServiceClient.Web;
using Base.Web.ViewModels;

namespace Base.Web.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            Session.RemoveAll();
            Session.Clear();
            Session.Abandon();
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel model, string returnUrl)
        {
            try
            {
                IUserAppService userService = new UserAppService();

                var user = userService.LoginUser(model.UserName, model.Password);
                if (user != null)
                {
                    Session["UserSession"] = user;
                    return RedirectToLocal(returnUrl);
                }
            }
            catch (WebServiceException e)
            {
                ModelState.AddModelError("", e.ErrorMessage);
                return View(model);
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [AllowAnonymous]
        public ActionResult LogOff()
        {
            return RedirectToAction("LogIn", "Account", new { ReturnUrl = "" } );
        }

        #region Helpers

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
        #endregion
    }
}