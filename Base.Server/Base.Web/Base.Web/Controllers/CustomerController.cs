﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Base.Web.ApplicationServices;
using Base.Web.ApplicationServices.Interfaces;
using Base.Web.Controllers.Base;
using Base.Web.Controllers.CustomActionResult;

namespace Base.Web.Controllers
{
    public class CustomerController : BaseController
    {
        readonly ICustomerAppService _customerAppService = new CustomerAppService();
        //
        // GET: /Customer/
        public ActionResult Index()
        {
            return View();
        }

        #region partialview
        public ActionResult List()
        {
            return PartialView("_List");
        }

        public ActionResult Create()
        {
            return PartialView("_Create");
        }

        #endregion

        #region methods

        [HttpGet]
        public JsonNetResult GetCustomers()
        {
            var customers = _customerAppService.GetActiveCustomers().ToList();

            return new JsonNetResult()
            {
                Data = new { Customers = customers, Count = customers.Count() }
            };
        }

        #endregion
    }
}