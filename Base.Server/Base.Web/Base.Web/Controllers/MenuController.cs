﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.EnterpriseServices;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Optimization;
using Base.Web.Controllers.Base;
using Base.Web.Controllers.CustomActionResult;
using Base.Web.ViewModels;

namespace Base.Web.Controllers
{
    public class MenuController : BaseController
    {

        [HttpGet]
        public JsonNetResult GenerateMenuItems()
        {
            var user = (User)Session["UserSession"];

            var customerGroup1 = new MenuGroup()
            {
                MenuId = 1,
                GroupId = 2
            };
            var customerGroup2 = new MenuGroup()
            {
                MenuId = 5,
                GroupId = 2
            };

            var customerGroups = new List<MenuGroup> {customerGroup1, customerGroup2};

            var menu1 = new Menu()
            {
                MenuId = 1,
                Title = "Customer",
                Url = "#",
                MenuGroups = customerGroups
            };
            var menu2 = new Menu()
            {
                MenuId = 2,
                Title = "Admin",
                Url = "#"
            };
            var menu3 = new Menu()
            {
                MenuId = 3,
                Title = "User Management",
                Url = "/User/Index",
                ParentMenuId = 2
            };
            var menu4 = new Menu()
            {
                MenuId = 4,
                Title = "Group Management",
                Url = "/Group/Index",
                ParentMenuId = 2
            };
            var menu5 = new Menu()
            {
                MenuId = 5,
                Title = "Customer Management",
                Url = "/Customer/Index",
                ParentMenuId = 1
            };
            //var menu6 = new Menu()
            //{
            //    MenuId = 6,
            //    Title = "fsgsdfgsdf",
            //    Url = "/Customer/Index",
            //    ParentMenuId = 4
            //};
            //var menu7 = new Menu()
            //{
            //    MenuId = 7,
            //    Title = "fsdfgsgsfd",
            //    Url = "/Customer/Index",
            //    ParentMenuId = 5
            //};

            var items = new List<Menu> { menu1, menu2, menu3, menu4, menu5 };

            var generateMenuItem = new GenerateMenu(items);

            var htmlString = user != null ? generateMenuItem.Display() : string.Empty;

            return new JsonNetResult()
            {
                Data = new { HtmlString = htmlString }
            };
        }
    }

    public class GenerateMenu
    {
        private readonly IList<Menu> _menus;
        public GenerateMenu(IList<Menu> menus)
        {
            _menus = menus;
        }

        private IEnumerable<Menu> GetParentMenu()
        {
            var items = _menus.Where(m => m.ParentMenuId == null);
            return items.ToList();
        }

        private IEnumerable<Menu> GetChildMenu(int itemMenuId)
        {
            var items = _menus.Where(m => m.ParentMenuId == itemMenuId);
            return items.ToList();
        }

        public string Display()
        {
            var htmlString = string.Empty;

            foreach (var menu in GetParentMenu())
            {
                var childMenus = GetChildMenu(menu.MenuId);
                if (childMenus.Any())
                {
                    htmlString +=
                        @"<li class='dropdown'><a href='" + menu.Url + @"' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-expanded='false'>" +
                        menu.Title + @"<span class='caret'></span></a>
                                        <ul class='dropdown-menu' role='menu' aria-labelledby='dropdownMenu'>";

                    foreach (var childMenu in childMenus)
                    {
                        var subMenus = GetChildMenu(childMenu.MenuId);

                        if (subMenus.Any())
                        {
                            htmlString +=
                                @"<li class='dropdown-submenu'><a href='" + childMenu.Url + @"' >" + childMenu.Title + @"</a>
                                        <ul class='dropdown-menu'>";
                            foreach (var subMenu in subMenus)
                            {
                                htmlString += @"<li><a href='" + subMenu.Url + "'>" + subMenu.Title + @"</a></li>";
                            }
                            htmlString += "</ul></li> ";
                        }
                        else
                        {
                            htmlString += @"<li><a href='" + childMenu.Url + "'>" + childMenu.Title + @"</a></li>";
                        }
                        
                    }

                    htmlString += "</ul></li> ";
                }
                else
                {
                    htmlString +=
                        @"<li class='dropdown'>
                            <a href='" + menu.Url + @"' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-expanded='false'>" +
                        menu.Title + @"<span class='caret'></span></a>
                        </li>";
                }

            }

            return htmlString;
        }
    }

    public class Menu
    {
        public int MenuId { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public int? ParentMenuId { get; set; }
        public IEnumerable<MenuGroup> MenuGroups { get; set; } 
    }

    public class MenuGroup
    {
        public int MenuId { get; set; }
        public long GroupId { get; set; }
    }
}