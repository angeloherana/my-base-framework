﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Metadata.Edm;
using System.Linq;
using System.Web;
using Base.Web.ApplicationServices;
using Base.Web.ApplicationServices.Interfaces;
using Base.Web.ViewModels;

namespace Base.Web.Controllers.Base
{
    public class MenuHelper
    {

        #region MockGenerateMenu
        //public static string GenerateMockMenuItems()
        //{
        //    var context = HttpContext.Current;
        //    var user = (User)context.Session["UserSession"];

        //    var generateMenuItem = new GenerateMockMenu(user);

        //    var htmlString = user != null ? generateMenuItem.Display() : string.Empty;
        //    return htmlString;
        //}
        #endregion

        public static string GenerateMenuItems()
        {
            IMenuAppService customerAppService = new MenuAppService();
            var context = HttpContext.Current;

            if (context.Session["UserSession"] == null) return string.Empty;

            var user = (User)context.Session["UserSession"];
            var userGroups = user.UserGroups.ToList();
            var htmlString = string.Empty;

            foreach (var menu in customerAppService.GetParentMenu(userGroups))
            {
                var childMenus = customerAppService.GetChildMenu(userGroups, menu.MenuId).ToList();
                if (childMenus.Any())
                {
                    htmlString +=
                        @"<li class='dropdown'><a href='" + menu.Url + @"' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-expanded='false'>" +
                        menu.Title + @"<span class='caret'></span></a>
                                        <ul class='dropdown-menu' role='menu' aria-labelledby='dropdownMenu'>";

                    foreach (var childMenu in childMenus)
                    {
                        var subMenus = customerAppService.GetChildMenu(userGroups, childMenu.MenuId).ToList();

                        if (subMenus.Any())
                        {
                            htmlString +=
                                @"<li class='dropdown-submenu'><a href='" + childMenu.Url + @"' >" + childMenu.Title + @"</a>
                                        <ul class='dropdown-menu'>";
                            htmlString = subMenus.Aggregate(htmlString, (current, subMenu) =>
                                current + (@"<li><a href='" + subMenu.Url + "'>" + subMenu.Title + @"</a></li>"));
                            htmlString += "</ul></li> ";
                        }
                        else
                        {
                            htmlString += @"<li><a href='" + childMenu.Url + "'>" + childMenu.Title + @"</a></li>";
                        }
                    }
                    htmlString += "</ul></li> ";
                }
                else
                {
                    htmlString +=
                        @"<li class='dropdown'>
                            <a href='" + menu.Url + @"' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-expanded='false'>" +
                        menu.Title + @"<span class='caret'></span></a>
                        </li>";
                }

            }
            return htmlString;
        }
    }


    #region MockMenu
    public class GenerateMockMenu
    {
        private readonly IList<Menu> _menus;
        private readonly User _user;
        public GenerateMockMenu(User user)
        {
            _user = user;

            var customerGroup1 = new MenuGroup()
            {
                MenuId = 1,
                GroupId = 3
            };
            var customerGroup2 = new MenuGroup()
            {
                MenuId = 5,
                GroupId = 3
            };

            var adminGroup1 = new MenuGroup()
            {
                MenuId = 1,
                GroupId = 2
            };
            var adminGroup2 = new MenuGroup()
            {
                MenuId = 2,
                GroupId = 2
            };

            var adminGroup3 = new MenuGroup()
            {
                MenuId = 3,
                GroupId = 2
            };
            var adminGroup4 = new MenuGroup()
            {
                MenuId = 4,
                GroupId = 2
            };
            var adminGroup5 = new MenuGroup()
            {
                MenuId = 5,
                GroupId = 2
            };

            var groups = new List<MenuGroup> { customerGroup1, customerGroup2, adminGroup1, adminGroup2, adminGroup3, adminGroup4, adminGroup5 };

            var menu1 = new Menu()
            {
                MenuId = 1,
                Title = "Customer",
                Url = "#",
                MenuGroups = groups
            };
            var menu2 = new Menu()
            {
                MenuId = 2,
                Title = "Admin",
                Url = "#",
                MenuGroups = groups
            };
            var menu3 = new Menu()
            {
                MenuId = 3,
                Title = "User Management",
                Url = "/User/Index",
                ParentMenuId = 2,
                MenuGroups = groups
            };
            var menu4 = new Menu()
            {
                MenuId = 4,
                Title = "Group Management",
                Url = "/Group/Index",
                ParentMenuId = 2,
                MenuGroups = groups
            };
            var menu5 = new Menu()
            {
                MenuId = 5,
                Title = "Customer Management",
                Url = "/Customer/Index",
                ParentMenuId = 1,
                MenuGroups = groups
            };
            var menu6 = new Menu()
            {
                MenuId = 6,
                Title = "fsgsdfgsdf",
                Url = "/Customer/Index",
                ParentMenuId = 4
            };
            var menu7 = new Menu()
            {
                MenuId = 7,
                Title = "fsdfgsgsfd",
                Url = "/Customer/Index",
                ParentMenuId = 5
            };

            var items = new List<Menu> { menu1, menu2, menu3, menu4, menu5 };
            _menus = items;
        }

        private IEnumerable<Menu> GetParentMenu()
        {
            var groups = _user.UserGroups.ToList();
            var allowedMenu = new List<Menu>();
            var items = _menus.Where(m => m.ParentMenuId == null).ToList();

            foreach (var item in items)
            {
                var tempItem = item;
                allowedMenu.AddRange(from menuGroup in item.MenuGroups.Where(mg => mg.MenuId == tempItem.MenuId)
                                     where groups.Select(g => g.GroupId).Contains(menuGroup.GroupId)
                                     select item);
            }
            return allowedMenu.ToList();
        }

        private IEnumerable<Menu> GetChildMenu(int itemMenuId)
        {
            var groups = _user.UserGroups;
            var allowedMenu = new List<Menu>();
            var items = _menus.Where(m => m.ParentMenuId == itemMenuId);

            foreach (var item in items)
            {
                var tempItem = item;
                allowedMenu.AddRange(from menuGroup in item.MenuGroups.Where(mg => mg.MenuId == tempItem.MenuId)
                                     where groups.Select(g => g.GroupId).Contains(menuGroup.GroupId)
                                     select item);
            }
            return allowedMenu.ToList();
        }

        public string Display()
        {
            var htmlString = string.Empty;

            foreach (var menu in GetParentMenu())
            {
                var childMenus = GetChildMenu(menu.MenuId).ToList();
                if (childMenus.Any())
                {
                    htmlString +=
                        @"<li class='dropdown'><a href='" + menu.Url + @"' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-expanded='false'>" +
                        menu.Title + @"<span class='caret'></span></a>
                                        <ul class='dropdown-menu' role='menu' aria-labelledby='dropdownMenu'>";

                    foreach (var childMenu in childMenus)
                    {
                        var subMenus = GetChildMenu(childMenu.MenuId).ToList();

                        if (subMenus.Any())
                        {
                            htmlString +=
                                @"<li class='dropdown-submenu'><a href='" + childMenu.Url + @"' >" + childMenu.Title + @"</a>
                                        <ul class='dropdown-menu'>";
                            htmlString = subMenus.Aggregate(htmlString, (current, subMenu) =>
                                current + (@"<li><a href='" + subMenu.Url + "'>" + subMenu.Title + @"</a></li>"));
                            htmlString += "</ul></li> ";
                        }
                        else
                        {
                            htmlString += @"<li><a href='" + childMenu.Url + "'>" + childMenu.Title + @"</a></li>";
                        }

                    }

                    htmlString += "</ul></li> ";
                }
                else
                {
                    htmlString +=
                        @"<li class='dropdown'>
                            <a href='" + menu.Url + @"' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-expanded='false'>" +
                        menu.Title + @"<span class='caret'></span></a>
                        </li>";
                }

            }

            return htmlString;
        }
    }
    #endregion
}