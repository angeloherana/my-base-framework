﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Base.Web.Controllers.Base
{
    public class BaseController : Controller
    { 
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var context = filterContext.HttpContext;

            if (context.Session != null && context.Session.Count > 0) return;
            var redirectTo = string.Format("~/Account/Login?ReturnUrl={0}", HttpUtility.UrlEncode(context.Request.RawUrl));
            filterContext.Result = new RedirectResult(redirectTo);
        }

	}
}