﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Base.Web.ApplicationServices;
using Base.Web.ApplicationServices.Interfaces;
using Base.Web.Controllers.Base;
using Base.Web.Controllers.CustomActionResult;

namespace Base.Web.Controllers
{
    public class GroupController : BaseController
    {
        readonly IGroupAppService _groupAppService = new GroupAppService();
        //
        // GET: /Group/
        public ActionResult Index()
        {
            return View();
        }

        #region partialview
        public ActionResult List()
        {
            return PartialView("_List");
        }

        public ActionResult Create()
        {
            return PartialView("_Create");
        }

        #endregion

        #region methods

        [HttpGet]
        public JsonNetResult GetGroups()
        {
            var groups = _groupAppService.GetActiveGroups().ToList();

            return new JsonNetResult()
            {
                Data = new { Groups = groups, Count = groups.Count() }
            };
        }

        #endregion
    }
}