﻿angular.module('app.Menu')
    .controller('menuDisplayCtrl',
    [
        '$scope', 'menuSrvc',
        function ($scope, menuSrvc) {
            var init = function() {
                menuSrvc.generateMenu().then(function (resp) {
                    $scope.htmlString = resp.data.HtmlString;
                });
            };
            init();
        }
    ]);