﻿angular.module('menuDirective', [])
    .controller('menuCtrl', [
        '$scope','menuSrvc',
        function($scope, menuSrvc) {
            var init = function() {
                menuSrvc.generateMenu().then(function(resp) {
                    $scope.htmlString = resp.data.HtmlString;
                });
            };
            init();
}])
.directive('menuSample', function () {
    return {
        restrict: 'E',
        transclude: true,
        templateUrl: '_DisplayMenu.cshtml'
    };
});