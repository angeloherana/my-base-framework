﻿'use strict';

var app = angular.module('app.Menu', ['ngRoute', 'ngSanitize']);

app.filter("sanitize", ['$sce', function ($sce) {
    return function (htmlCode) {
        return $sce.trustAsHtml(htmlCode);
    };
}]);