﻿angular.module('app.Menu')
    .service('menuSrvc', ['$http', 
        function ($http) {
        var generateMenu = function () {
            return $http.get('/Menu/GenerateMenuItems');
            };
        return {
            generateMenu: generateMenu
        };
        }
    ]);