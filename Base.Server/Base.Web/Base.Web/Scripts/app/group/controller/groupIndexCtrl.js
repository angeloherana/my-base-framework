﻿angular.module('app.Group')
    .controller('groupIndexCtrl',
    [
        '$scope', 'groupSrvc',
        function ($scope, groupSrvc) {
            var init = function() {
                groupSrvc.getGroups().then(function(resp) {
                    $scope.groups = resp.data.Groups;
                    console.log($scope.groups);
                });
            };
            init();
        }
    ]);