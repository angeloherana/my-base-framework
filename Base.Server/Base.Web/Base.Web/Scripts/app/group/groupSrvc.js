﻿angular.module('app.Group')
    .service('groupSrvc', ['$http', 
        function ($http) {
        var getGroups = function () {
            return $http.get('/Group/GetGroups');
            },
         saveGroup = function (groupDetail) {
             return $http.post('/Group/Save', groupDetail);
        };
        return {
            getGroups: getGroups,
            saveGroup: saveGroup,
        };
        }
    ]);