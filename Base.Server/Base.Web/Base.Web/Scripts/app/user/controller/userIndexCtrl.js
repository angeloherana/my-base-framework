﻿angular.module('app.User')
    .controller('userIndexCtrl',
    [
        '$scope', 'userSrvc',
        function ($scope, userSrvc) {
            var init = function() {
                userSrvc.getUsers().then(function(resp) {
                    $scope.users = resp.data.Users;
                    console.log($scope.users);
                });
            };
            init();
        }
    ]);