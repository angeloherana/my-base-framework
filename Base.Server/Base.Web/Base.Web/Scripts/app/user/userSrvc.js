﻿angular.module('app.User')
    .service('userSrvc', ['$http', 
        function ($http) {
        var getUsers = function () {
            return $http.get('/User/GetUsers');
            },
         saveUser = function (userDetail) {
             return $http.post('/User/Save', userDetail);
        };
        return {
            getUsers: getUsers,
            saveUser: saveUser,
        };
        }
    ]);