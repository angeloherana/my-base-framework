﻿angular.module('app.Customer')
    .service('customerSrvc', ['$http', 
        function ($http) {
        var getCustomers = function () {
            return $http.get('/Customer/GetCustomers');
            },
         saveCustomer = function (customerDetail) {
             return $http.post('/Customer/Save', customerDetail);
        };
        return {
            getCustomers: getCustomers,
            saveCustomer: saveCustomer,
        };
        }
    ]);