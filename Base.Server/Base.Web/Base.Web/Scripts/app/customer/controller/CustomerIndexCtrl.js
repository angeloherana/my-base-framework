﻿angular.module('app.Customer')
    .controller('customerIndexCtrl',
    [
        '$scope', 'customerSrvc',
        function ($scope, customerSrvc) {
            var init = function() {
                customerSrvc.getCustomers().then(function (resp) {
                    $scope.customers = resp.data.Customers;
                    console.log($scope.customers);
                });
            };
            init();
        }
    ]);