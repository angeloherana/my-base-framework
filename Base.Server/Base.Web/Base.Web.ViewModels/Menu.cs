﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base.Web.ViewModels
{
    public class Menu
    {
        public long MenuId { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public long? ParentMenuId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public IEnumerable<MenuGroup> MenuGroups { get; set; }
    }
}
