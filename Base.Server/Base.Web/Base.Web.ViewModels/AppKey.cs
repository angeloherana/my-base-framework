﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base.Web.ViewModels
{
    public class AppKey
    {
        public long AppKeyId { get; set; }
        public string Name { get; set; }
        public string ApiKey { get; set; }
    }
}
