﻿using System.Configuration;
using ServiceStack.ServiceClient.Web;

namespace Base.Web.Common
{
    public static class BaseServiceClient
    {
        public static JsonServiceClient GetClient()
        {
            var client = new JsonServiceClient(ConfigurationManager.AppSettings["API_URL"])
            {
                LocalHttpWebRequestFilter =
                    req => req.Headers.Add("X-ApiKey", ConfigurationManager.AppSettings["ApiKey"]),
            };
            return client;
        }
    }
}
