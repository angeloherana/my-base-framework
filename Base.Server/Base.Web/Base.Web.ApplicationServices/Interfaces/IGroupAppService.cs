﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.Web.ViewModels;

namespace Base.Web.ApplicationServices.Interfaces
{
    public interface IGroupAppService
    {
        IEnumerable<Group> GetActiveGroups();
        Group GetGroupByGroupId(long groupId);
        Group CreateGroup(Group group);
        Group UpdateGroup(Group group);
        bool DeleteGroup(long groupId);
    }
}
