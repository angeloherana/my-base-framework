﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.Web.ViewModels;

namespace Base.Web.ApplicationServices.Interfaces
{
    public interface IUserAppService
    {
        IEnumerable<User> GetActiveUsers();
        User GetUserByUserId(long userId);
        User LoginUser(string userName, string password);
        User CreateUser(User user);
        User UpdateUser(User user);
        bool DeleteUser(long userId);
    }
}
