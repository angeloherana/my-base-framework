﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.Web.ViewModels;

namespace Base.Web.ApplicationServices.Interfaces
{
    public interface IMenuAppService
    {
        IEnumerable<Menu> GetParentMenu(IList<UserGroup> userGroups);
        IEnumerable<Menu> GetChildMenu(IList<UserGroup> userGroups, long parentMenuId);
    }
}
