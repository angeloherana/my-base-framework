﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.Web.ViewModels;

namespace Base.Web.ApplicationServices.Interfaces
{
    public interface IAppKeyAppService
    {
        AppKey GenerateAppKey(AppKey appKey);
    }
}
