﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.Web.ViewModels;

namespace Base.Web.ApplicationServices.Interfaces
{
    public interface ICustomerAppService
    {
        IEnumerable<Customer> GetActiveCustomers();
        Customer GetCustomerByCustomerId(long customerId);
        Customer CreateCustomer(Customer customer);
        Customer UpdateCustomer(Customer customer);
        bool DeleteCustomer(long customerId);
    }
}
