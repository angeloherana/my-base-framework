﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Base.Web.Common;
using ServiceStack.ServiceClient.Web;
using Base.Server.API.ServiceModel.User;
using Base.Web.ApplicationServices.Interfaces;
using Base.Web.ViewModels;
using Base.Server.API.DTO;

namespace Base.Web.ApplicationServices
{
    public class UserAppService : IUserAppService
    {
        public IEnumerable<User> GetActiveUsers()
        {
            var client = BaseServiceClient.GetClient();

            Mapper.CreateMap<UserDTO, User>();
            Mapper.CreateMap<User, UserDTO>();

            var response = client.Get(new GetActiveUsers());

            return Mapper.Map<IList<User>>(response.Users);
        }

        public User GetUserByUserId(long userId)
        {
            var client = BaseServiceClient.GetClient();

            Mapper.CreateMap<UserDTO, User>();
            Mapper.CreateMap<User, UserDTO>();

            var response = client.Get(new GetUserByUserId()
            {
                UserId = userId
            });

            return Mapper.Map<User>(response);
        }

        public User LoginUser(string userName, string password)
        {
            var client = BaseServiceClient.GetClient();

            Mapper.CreateMap<LoginUserResponse, User>();
            Mapper.CreateMap<User, LoginUserResponse>();
            Mapper.CreateMap<UserDTO, User>();
            Mapper.CreateMap<User, UserDTO>();
            Mapper.CreateMap<UserGroupDTO, UserGroup>();
            Mapper.CreateMap<UserGroup, UserGroupDTO>();

            var response = client.Get(new LoginUser()
            {
                UserName = userName,
                Password = password
            });
            return Mapper.Map<User>(response.User);
        }

        public User CreateUser(User user)
        {
            var client = BaseServiceClient.GetClient();

            Mapper.CreateMap<UserDTO, User>();
            Mapper.CreateMap<User, UserDTO>();

            var response = client.Post(new CreateUser()
            {
                User = Mapper.Map<UserDTO>(user) 
            });

            return Mapper.Map<User>(response);
        }

        public User UpdateUser(User user)
        {
            var client = BaseServiceClient.GetClient();

            Mapper.CreateMap<UserDTO, User>();
            Mapper.CreateMap<User, UserDTO>();

            var response = client.Post(new UpdateUser()
            {
                User = Mapper.Map<UserDTO>(user)
            });

            return Mapper.Map<User>(response);
        }

        public bool DeleteUser(long userId)
        {
            var client = BaseServiceClient.GetClient();

            Mapper.CreateMap<UserDTO, User>();
            Mapper.CreateMap<User, UserDTO>();

            var response = client.Post(new DeleteUser()
            {
                UserId = userId
            });

            return response.IsDeleted;
        }
    }
}
