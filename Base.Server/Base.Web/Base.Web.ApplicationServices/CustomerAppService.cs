﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Base.Server.API.ServiceModel.Customer;
using Base.Web.ApplicationServices.Interfaces;
using Base.Web.Common;
using Base.Web.ViewModels;
using Base.Server.API.DTO;

namespace Base.Web.ApplicationServices
{
    public class CustomerAppService : ICustomerAppService
    {
        public IEnumerable<Customer> GetActiveCustomers()
        {
            var client = BaseServiceClient.GetClient();

            Mapper.CreateMap<CustomerDTO, Customer>();
            Mapper.CreateMap<Customer, CustomerDTO>();

            var response = client.Get(new GetActiveCustomers());

            return Mapper.Map<IList<Customer>>(response.Customers);
        }

        public Customer GetCustomerByCustomerId(long customerId)
        {
            var client = BaseServiceClient.GetClient();

            Mapper.CreateMap<CustomerDTO, Customer>();
            Mapper.CreateMap<Customer, CustomerDTO>();

            var response = client.Get(new GetCustomerByCustomerId()
            {
                CustomerId = customerId
            });

            return Mapper.Map<Customer>(response);
        }

        public Customer CreateCustomer(Customer customer)
        {
            var client = BaseServiceClient.GetClient();

            Mapper.CreateMap<CustomerDTO, Customer>();
            Mapper.CreateMap<Customer, CustomerDTO>();

            var response = client.Post(new CreateCustomer()
            {
                Customer = Mapper.Map<CustomerDTO>(customer)
            });

            return Mapper.Map<Customer>(response);
        }

        public Customer UpdateCustomer(Customer customer)
        {
            var client = BaseServiceClient.GetClient();

            Mapper.CreateMap<CustomerDTO, Customer>();
            Mapper.CreateMap<Customer, CustomerDTO>();

            var response = client.Post(new UpdateCustomer()
            {
                Customer = Mapper.Map<CustomerDTO>(customer)
            });

            return Mapper.Map<Customer>(response);
        }

        public bool DeleteCustomer(long customerId)
        {
            var client = BaseServiceClient.GetClient();

            Mapper.CreateMap<CustomerDTO, Customer>();
            Mapper.CreateMap<Customer, CustomerDTO>();

            var response = client.Post(new DeleteCustomer()
            {
                CustomerId = customerId
            });

            return response.IsDeleted;
        }
    }
}
