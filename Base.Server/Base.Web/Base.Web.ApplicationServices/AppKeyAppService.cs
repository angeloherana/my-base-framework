﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Base.Server.API.DTO;
using Base.Server.API.ServiceModel.AppKey;
using Base.Web.ApplicationServices.Interfaces;
using Base.Web.Common;
using ServiceStack.ServiceClient.Web;
using Base.Web.ViewModels;

namespace Base.Web.ApplicationServices
{
    public class AppKeyAppService : IAppKeyAppService
    {
        public AppKey GenerateAppKey(AppKey appKey)
        {
            var client = BaseServiceClient.GetClient();

            Mapper.CreateMap<AppKeyDTO, AppKey>();
            Mapper.CreateMap<AppKey, AppKeyDTO>();

            var response = client.Post(new CreateAppKey()
            {
                AppKey = Mapper.Map<AppKeyDTO>(appKey)
            });

            return Mapper.Map<AppKey>(response);
        }
    }
}
