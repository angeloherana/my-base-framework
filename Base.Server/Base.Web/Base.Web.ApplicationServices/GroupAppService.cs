﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Base.Server.API.ServiceModel.Group;
using Base.Web.ApplicationServices.Interfaces;
using Base.Web.Common;
using Base.Web.ViewModels;
using Base.Server.API.DTO;

namespace Base.Web.ApplicationServices
{
    public class GroupAppService : IGroupAppService
    {
        public IEnumerable<Group> GetActiveGroups()
        {
            var client = BaseServiceClient.GetClient();

            Mapper.CreateMap<GroupDTO, Group>();
            Mapper.CreateMap<Group, GroupDTO>();

            var response = client.Get(new GetActiveGroups());

            return Mapper.Map<IList<Group>>(response.Groups);
        }

        public Group GetGroupByGroupId(long groupId)
        {
            var client = BaseServiceClient.GetClient();

            Mapper.CreateMap<GroupDTO, Group>();
            Mapper.CreateMap<Group, GroupDTO>();

            var response = client.Get(new GetGroupByGroupId()
            {
                GroupId = groupId
            });

            return Mapper.Map<Group>(response);
        }

        public Group CreateGroup(Group group)
        {
            var client = BaseServiceClient.GetClient();

            Mapper.CreateMap<GroupDTO, Group>();
            Mapper.CreateMap<Group, GroupDTO>();

            var response = client.Post(new CreateGroup()
            {
                Group = Mapper.Map<GroupDTO>(group)
            });

            return Mapper.Map<Group>(response);
        }

        public Group UpdateGroup(Group group)
        {
            var client = BaseServiceClient.GetClient();

            Mapper.CreateMap<GroupDTO, Group>();
            Mapper.CreateMap<Group, GroupDTO>();

            var response = client.Post(new UpdateGroup()
            {
                Group = Mapper.Map<GroupDTO>(group)
            });

            return Mapper.Map<Group>(response);
        }

        public bool DeleteGroup(long groupId)
        {
            var client = BaseServiceClient.GetClient();

            Mapper.CreateMap<GroupDTO, Group>();
            Mapper.CreateMap<Group, GroupDTO>();

            var response = client.Post(new DeleteGroup()
            {
                GroupId = groupId
            });

            return response.IsDeleted;
        }
    }
}
