﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Base.Server.API.DTO;
using Base.Server.API.ServiceModel.Menu;
using Base.Web.ApplicationServices.Interfaces;
using Base.Web.Common;
using Base.Web.ViewModels;

namespace Base.Web.ApplicationServices
{
    public class MenuAppService : IMenuAppService
    {
        public IEnumerable<Menu> GetParentMenu(IList<UserGroup> userGroups)
        {
            var client = BaseServiceClient.GetClient();

            Mapper.CreateMap<Menu, MenuDTO>();
            Mapper.CreateMap<MenuDTO, Menu>();
            Mapper.CreateMap<MenuGroup, MenuGroupDTO>();
            Mapper.CreateMap<MenuGroupDTO, MenuGroup>();


            var response = client.Get(new GetParentMenuByGroup()
            {
                UserGroups = Mapper.Map<IList<UserGroupDTO>>(userGroups)
            });

            return Mapper.Map<IList<Menu>>(response.Menus);
        }
        public IEnumerable<Menu> GetChildMenu(IList<UserGroup> userGroups, long parentMenuId)
        {
            var client = BaseServiceClient.GetClient();

            Mapper.CreateMap<Menu, MenuDTO>();
            Mapper.CreateMap<MenuDTO, Menu>();
            Mapper.CreateMap<MenuGroup, MenuGroupDTO>();
            Mapper.CreateMap<MenuGroupDTO, MenuGroup>();


            var response = client.Get(new GetChildMenuByGroup()
            {
                UserGroups = Mapper.Map<IList<UserGroupDTO>>(userGroups),
                ParentMenuId = parentMenuId
            });

            return Mapper.Map<IList<Menu>>(response.Menus);
        }
    }
}
