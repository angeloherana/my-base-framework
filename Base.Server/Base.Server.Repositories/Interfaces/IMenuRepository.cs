﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.Server.Models;

namespace Base.Server.Repositories.Interfaces
{
    public interface IMenuRepository
    {
        IEnumerable<Menu> GetParentMenus();
        IEnumerable<Menu> GetChildMenus(long parentMenuId);
    }
}
