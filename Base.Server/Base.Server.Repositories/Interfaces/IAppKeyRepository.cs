﻿using Base.Server.Models;

namespace Base.Server.Repositories.Interfaces
{
    public interface IAppKeyRepository : IRepository<AppKey>
    {
        AppKey GetAppKeyByApiKey(string apiKey);
    }
}
