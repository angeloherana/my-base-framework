namespace Base.Server.Repositories.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedRestrictionTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Restriction",
                c => new
                    {
                        RestrictionId = c.Long(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        CreatedBy = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        UpdatedBy = c.String(),
                        UpdatedDate = c.DateTime(),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.RestrictionId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Restriction");
        }
    }
}
