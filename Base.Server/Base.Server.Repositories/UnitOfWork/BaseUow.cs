﻿using System;
using Base.Server.Repositories.Implementations;
using Base.Server.Repositories.Interfaces;
using Base.Server.Repositories.Providers;

namespace Base.Server.Repositories.UnitOfWork
{
    public class BaseUow : IBaseUow, IDisposable
    {
        private BaseDbContext DbContext { get; set; }
        protected IRepositoryProvider RepositoryProvider { get; set; }

        public BaseUow(IRepositoryProvider repositoryProvider)
        {
            CreateDbContext();
            repositoryProvider.DbContext = DbContext;
            RepositoryProvider = repositoryProvider;
        }


        public IAppKeyRepository AppKeyRepository { get { return GetRepo<IAppKeyRepository>(); } }
        public IUserRepository UserRepository { get { return GetRepo<IUserRepository>(); } }
        public IGroupRepository GroupRepository { get { return GetRepo<IGroupRepository>(); } }
        public IUserGroupRepository UserGroupRepository { get { return GetRepo<IUserGroupRepository>(); } }
        public ICustomerRepository CustomerRepository { get { return GetRepo<ICustomerRepository>(); } }
        public IMenuRepository MenuRepository { get { return GetRepo<IMenuRepository>(); } }
        public IMenuGroupRepository MenuGroupRepository { get { return GetRepo<IMenuGroupRepository>(); } }
        public IRestrictionRepository RestrictionRepository { get { return GetRepo<IRestrictionRepository>(); } }

        public void Commit()
        {
            DbContext.SaveChanges();
        }

        protected void CreateDbContext()
        {
            DbContext = new BaseDbContext();

            // Do NOT enable proxied entities, else serialization fails
            DbContext.Configuration.ProxyCreationEnabled = false;

            // Load navigation properties explicitly (avoid serialization trouble)
            DbContext.Configuration.LazyLoadingEnabled = false;

            // Because Web API will perform validation, we don't need/want EF to do so
            DbContext.Configuration.ValidateOnSaveEnabled = false;

            //DbContext.Configuration.AutoDetectChangesEnabled = true;
            // We won't use this performance tweak because we don't need 
            // the extra performance and, when autodetect is false,
            // we'd have to be careful. We're not being that careful.
        }

        #region DISPOSAL
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposing) return;
            if (DbContext != null)
            {
                DbContext.Dispose();
            }
        }

        #endregion


        private IRepository<T> GetStandardRepo<T>() where T : class
        {
            return RepositoryProvider.GetRepositoryForEntityType<T>();
        }
        private T GetRepo<T>() where T : class
        {
            return RepositoryProvider.GetRepository<T>();
        }


    }
}
