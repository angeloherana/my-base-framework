﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Conventions;
using Base.Server.Models;
using Base.Server.Repositories.Implementations;

namespace Base.Server.Repositories.UnitOfWork
{
    public class BaseDbContext : DbContext
    {
        public BaseDbContext()
            : base("BaseContext") { }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // Use singular table names
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            //modelBuilder.Configurations.Add(new )
            //modelBuilder.Configurations.Add(new UserAccountMap());
            //modelBuilder.Configurations.Add(new UserMap());
        }

        public DbSet<AppKey> AppKeys { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<UserGroup> UserGroups { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Menu> Menus { get; set; }
        public DbSet<MenuGroup> MenuGroups { get; set; }
        public DbSet<Restriction> Restrictions { get; set; }

    }


}
