﻿using System.Runtime.Remoting.Messaging;
using Base.Server.Models;
using Base.Server.Repositories.Implementations;
using Base.Server.Repositories.Interfaces;

namespace Base.Server.Repositories.UnitOfWork
{
    public interface IBaseUow
    {
        // Save pending changes to the data store.
        void Commit();

        // Repositories
        IAppKeyRepository AppKeyRepository { get; }
        IUserRepository UserRepository { get; }
        IGroupRepository GroupRepository { get; }
        IUserGroupRepository UserGroupRepository { get; }
        ICustomerRepository CustomerRepository { get; }
        IMenuRepository MenuRepository { get; }
        IMenuGroupRepository MenuGroupRepository { get; }
        IRestrictionRepository RestrictionRepository { get; }

        void Dispose();
    }
}
