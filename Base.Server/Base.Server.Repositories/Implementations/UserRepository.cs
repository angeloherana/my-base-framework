﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Base.Server.Models;
using Base.Server.Repositories.Interfaces;
using System.Data.Entity;

namespace Base.Server.Repositories.Implementations
{
    public class UserRepository : EFRepository<User>, IUserRepository
    {
        public UserRepository(DbContext dbContext) : base(dbContext) { }

        public User GetUserByUserId(long userId)
        {
            return DbSet.Where(u => u.UserId == userId).Include(u => u.UserGroups).SingleOrDefault();
            
        }

        public IEnumerable<User> GetActiveUsers()
        {
            return DbSet.Where(u => u.IsActive).Include(u => u.UserGroups);
        }

        public User GetUserByUserNamePassword(string userName, string password)
        {
            var result = DbSet.Where(u => u.UserName == userName && u.Password == password).Include(u => u.UserGroups);
            return result.FirstOrDefault();
        }
    }
}
