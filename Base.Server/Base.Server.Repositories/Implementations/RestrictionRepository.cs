﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.Server.Models;
using Base.Server.Repositories.Interfaces;

namespace Base.Server.Repositories.Implementations
{
    public class RestrictionRepository : EFRepository<Restriction>, IRestrictionRepository
    {
        public RestrictionRepository(DbContext dbContext) : base(dbContext) { }

        public IEnumerable<Restriction> GetActiveRestrictions()
        {
            return DbSet.Where(g => g.IsActive);
        }
    }
}
