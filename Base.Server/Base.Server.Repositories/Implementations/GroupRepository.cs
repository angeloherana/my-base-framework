﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.Server.Models;
using Base.Server.Repositories.Interfaces;

namespace Base.Server.Repositories.Implementations
{
    public class GroupRepository : EFRepository<Group>, IGroupRepository
    {
        public GroupRepository(DbContext dbContext) : base(dbContext) { }
        public IEnumerable<Group> GetActiveGroups()
        {
            return DbSet.Where(g => g.IsActive);
        }
    }
}
