﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.Server.Models;
using Base.Server.Repositories.Interfaces;

namespace Base.Server.Repositories.Implementations
{
    public class MenuRepository  : EFRepository<Menu>, IMenuRepository
    {
        public MenuRepository(DbContext dbContext) : base(dbContext) { }

        public IEnumerable<Menu> GetParentMenus()
        {
            return DbSet.Where(m => m.ParentMenuId == null)
                .Include(mg => mg.MenuGroups);
        } 
        public IEnumerable<Menu> GetChildMenus(long parentMenuId)
        {
            return DbSet.Where(m => m.ParentMenuId == parentMenuId)
                .Include(mg => mg.MenuGroups);
        }
    }
}
