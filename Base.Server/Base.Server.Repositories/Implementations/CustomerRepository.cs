﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.Server.Models;
using Base.Server.Repositories.Interfaces;

namespace Base.Server.Repositories.Implementations
{
    public class CustomerRepository : EFRepository<Customer>, ICustomerRepository
    {
        public CustomerRepository(DbContext dbContext) : base(dbContext) { }
        public IEnumerable<Customer> GetActiveCustomers()
        {
            return DbSet.Where(c => c.IsActive);
        }
    }
}
