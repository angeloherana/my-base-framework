﻿using System.Data.Entity;
using System.Linq;
using Base.Server.Models;
using Base.Server.Repositories.Interfaces;
using System.Data.Entity;

namespace Base.Server.Repositories.Implementations
{
    public class AppKeyRepository : EFRepository<AppKey>, IAppKeyRepository
    {
        public AppKeyRepository(DbContext dbContext) : base(dbContext) { }

        public AppKey GetAppKeyByApiKey(string apiKey)
        {
            return DbSet.SingleOrDefault(c => c.ApiKey == apiKey);
        }
    }
}
