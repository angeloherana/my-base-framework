﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.Server.Models;
using Base.Server.Repositories.Interfaces;

namespace Base.Server.Repositories.Implementations
{
    public class UserGroupRepository : EFRepository<UserGroup>, IUserGroupRepository
    {
        public UserGroupRepository(DbContext dbContext) : base(dbContext) { }
    }
}
