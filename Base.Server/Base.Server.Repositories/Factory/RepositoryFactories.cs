﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using Base.Server.Models;
using Base.Server.Repositories.Implementations;
using Base.Server.Repositories.Interfaces;

namespace Base.Server.Repositories.Factory
{
    public class RepositoryFactories
    {
        /// <summary>
        /// Return the runtime WORLDSMART SERVER repository factory functions,
        /// each one is a factory for a repository of a particular type.
        /// </summary>
        /// <remarks>
        /// MODIFY THIS METHOD TO ADD CUSTOM WORLDSMART SERVER FACTORY FUNCTIONS
        /// </remarks>
        private IDictionary<Type, Func<DbContext, object>> GetBaseFactories()
        {
            return new Dictionary<Type, Func<DbContext, object>>
                {
                   {typeof(IAppKeyRepository), dbContext => new AppKeyRepository(dbContext)},
                   {typeof(IUserRepository), dbContext => new UserRepository(dbContext)},
                   {typeof(IGroupRepository), dbContext => new GroupRepository(dbContext)},
                   {typeof(IUserGroupRepository), dbContext => new UserGroupRepository(dbContext)},
                   {typeof(ICustomerRepository), dbContext => new CustomerRepository(dbContext)},
                   {typeof(IMenuRepository), dbContext => new MenuRepository(dbContext)},
                   {typeof(IMenuGroupRepository), dbContext => new MenuGroupRepository(dbContext)},
                   {typeof(IRestrictionRepository), dbContext => new RestrictionRepository(dbContext)},
                };
        }

        /// <summary>
        /// Constructor that initializes with runtime Base Server repository factories
        /// </summary>
        public RepositoryFactories()
        {
            _repositoryFactories = GetBaseFactories();
        }

        /// <summary>
        /// Constructor that initializes with an arbitrary collection of factories
        /// </summary>
        /// <param name="factories">
        /// The repository factory functions for this instance. 
        /// </param>
        /// <remarks>
        /// This ctor is primarily useful for testing this class
        /// </remarks>
        public RepositoryFactories(IDictionary<Type, Func<DbContext, object>> factories)
        {
            _repositoryFactories = factories;
        }

        /// <summary>
        /// Get the repository factory function for the type.
        /// </summary>
        /// <typeparam name="T">Type serving as the repository factory lookup key.</typeparam>
        /// <returns>The repository function if found, else null.</returns>
        /// <remarks>
        /// The type parameter, T, is typically the repository type 
        /// but could be any type (e.g., an entity type)
        /// </remarks>
        public Func<DbContext, object> GetRepositoryFactory<T>()
        {

            Func<DbContext, object> factory;
            _repositoryFactories.TryGetValue(typeof(T), out factory);
            return factory;
        }

        /// <summary>
        /// Get the factory for <see cref="IRepository{T}"/> where T is an entity type.
        /// </summary>
        /// <typeparam name="T">The root type of the repository, typically an entity type.</typeparam>
        /// <returns>
        /// A factory that creates the <see cref="IRepository{T}"/>, given an EF <see cref="DbContext"/>.
        /// </returns>
        /// <remarks>
        /// Looks first for a custom factory in <see cref="_repositoryFactories"/>.
        /// If not, falls back to the <see cref="DefaultEntityRepositoryFactory{T}"/>.
        /// You can substitute an alternative factory for the default one by adding
        /// a repository factory for type "T" to <see cref="_repositoryFactories"/>.
        /// </remarks>
        public Func<DbContext, object> GetRepositoryFactoryForEntityType<T>() where T : class
        {
            return GetRepositoryFactory<T>() ?? DefaultEntityRepositoryFactory<T>();
        }

        /// <summary>
        /// Default factory for a <see cref="IRepository{T}"/> where T is an entity.
        /// </summary>
        /// <typeparam name="T">Type of the repository's root entity</typeparam>
        protected virtual Func<DbContext, object> DefaultEntityRepositoryFactory<T>() where T : class
        {
            return dbContext => new EFRepository<T>(dbContext);
        }

        /// <summary>
        /// Get the dictionary of repository factory functions.
        /// </summary>
        /// <remarks>
        /// A dictionary key is a System.Type, typically a repository type.
        /// A value is a repository factory function
        /// that takes a <see cref="DbContext"/> argument and returns
        /// a repository object. Caller must know how to cast it.
        /// </remarks>
        private readonly IDictionary<Type, Func<DbContext, object>> _repositoryFactories;

    }
}
