﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Base.Server.Infrastructure.Services.Interfaces;

namespace Base.Server.Infrastructure.Services
{
    public class EncryptService : IEncryptService
    {
        public string Encrypt(string textToEncrypt)
        {
            var sha256 = SHA256.Create();

            //convert the input text to array of bytes
            var hashData = sha256.ComputeHash(Encoding.Default.GetBytes(textToEncrypt));

            //create new instance of StringBuilder to save hashed data
            var encryptedKey = new StringBuilder();

            //loop for each byte and add it to StringBuilder
            foreach (var t in hashData)
            {
                encryptedKey.Append(t.ToString());
            }

            // return hexadecimal string
            return encryptedKey.ToString();
        }
    }
}
