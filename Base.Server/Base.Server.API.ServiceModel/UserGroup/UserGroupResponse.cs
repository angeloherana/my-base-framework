﻿using System.Collections.Generic;
using ServiceStack;
using ServiceStack.ServiceInterface.ServiceModel;
using Base.Server.API.DTO;

namespace Base.Server.API.ServiceModel.UserGroup
{
    public class GetAllUserGroupsResponse : ResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
        public IEnumerable<UserGroupDTO> UserGroups { get; set; }
    }
    public class CreateUserGroupResponse : ResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
        public UserGroupDTO UserGroup { get; set; }
    }
    public class DeleteUserGroupResponse : ResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
        public bool IsDeleted { get; set; }
    }
}
