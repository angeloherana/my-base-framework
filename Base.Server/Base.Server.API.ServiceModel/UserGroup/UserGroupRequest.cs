﻿using ServiceStack;
using ServiceStack.ServiceHost;
using Base.Server.API.DTO;
using Base.Server.API.ServiceModel.Customer;

namespace Base.Server.API.ServiceModel.UserGroup
{

    [Route("/UserGroup/GetAllUserGroups", "GET")]
    public class GetAllUserGroups : IReturn<GetAllUserGroupsResponse>
    {
    }

    [Route("/UserGroup/CreateUser", "POST")]
    public class CreateUserGroup : IReturn<CreateUserGroupResponse>
    {
        public UserGroupDTO UserGroup { get; set; }
    }

    [Route("/UserGroup/DeleteUserGroup", "POST")]
    public class DeleteUserGroup : IReturn<DeleteUserGroupResponse>
    {
        public long UserGroupId { get; set; }
    }
}
