﻿using ServiceStack;
using ServiceStack.ServiceHost;
using Base.Server.API.DTO;

namespace Base.Server.API.ServiceModel.AppKey
{
    [Route("/AppKey/CreateAppKey", "POST")]
    public class CreateAppKey : IReturn<CreateAppKeyResponse>
    {
        public AppKeyDTO AppKey { get; set; }
    }

    [Route("/AppKey/GetAllAppKey", "GET")]
    public class GetAllAppKey : IReturn<GetAllAppKeyResponse>
    {
    }
}
