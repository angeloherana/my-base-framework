﻿using System.Collections.Generic;
using ServiceStack;
using ServiceStack.ServiceInterface.ServiceModel;
using Base.Server.API.DTO;

namespace Base.Server.API.ServiceModel.AppKey
{
    public class CreateAppKeyResponse : ResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
        public AppKeyDTO AppKey { get; set; }
    }

    public class GetAllAppKeyResponse : ResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
        public IEnumerable<AppKeyDTO> AppKeys { get; set; }
    }
}
