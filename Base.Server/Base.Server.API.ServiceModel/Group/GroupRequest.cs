﻿using ServiceStack;
using ServiceStack.ServiceHost;
using Base.Server.API.DTO;

namespace Base.Server.API.ServiceModel.Group
{

    [Route("/Group/GetActiveGroups", "GET")]
    public class GetActiveGroups : IReturn<GetActiveGroupsResponse>
    {
    }

    [Route("/Group/GetGroupByGroupId", "GET")]
    public class GetGroupByGroupId : IReturn<GetGroupByGroupIdResponse>
    {
        public long GroupId { get; set; }
    }

    [Route("/Group/CreateGroup", "POST")]
    public class CreateGroup : IReturn<CreateGroupResponse>
    {
        public GroupDTO Group { get; set; }
    }

    [Route("/Group/UpdateGroup", "POST")]
    public class UpdateGroup : IReturn<UpdateGroupResponse>
    {
        public GroupDTO Group { get; set; }
    }

    [Route("/Group/DeleteGroup", "POST")]
    public class DeleteGroup : IReturn<DeleteGroupResponse>
    {
        public long GroupId { get; set; }
        public string UpdatedBy { get; set; }
    }
}
