﻿using System.Collections.Generic;
using ServiceStack;
using ServiceStack.ServiceInterface.ServiceModel;
using Base.Server.API.DTO;

namespace Base.Server.API.ServiceModel.Group
{
    public class GetActiveGroupsResponse : ResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
        public IEnumerable<GroupDTO> Groups { get; set; }
    }
    public class GetGroupByGroupIdResponse : ResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
        public GroupDTO Group { get; set; }
    }
    public class CreateGroupResponse : ResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
        public GroupDTO Group { get; set; }
    }
    public class UpdateGroupResponse : ResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
        public GroupDTO Group { get; set; }
    }
    public class DeleteGroupResponse : ResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
        public bool IsDeleted { get; set; }
    }
}
