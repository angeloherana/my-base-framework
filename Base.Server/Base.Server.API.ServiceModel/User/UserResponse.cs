﻿using System.Collections.Generic;
using ServiceStack;
using ServiceStack.ServiceInterface.ServiceModel;
using Base.Server.API.DTO;

namespace Base.Server.API.ServiceModel.User
{
    public class GetActiveUsersResponse : ResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
        public IEnumerable<UserDTO> Users { get; set; }
    }
    public class GetUserByUserIdResponse
    {
        public ResponseStatus ResponseStatus { get; set; }
        public UserDTO User { get; set; }
    }
    public class LoginUserResponse
    {
        public ResponseStatus ResponseStatus { get; set; }
        public UserDTO User { get; set; }
    }
    public class CreateUserResponse : ResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
        public UserDTO User { get; set; }
    }
    public class UpdateUserResponse : ResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
        public UserDTO User { get; set; }
    }
    public class DeleteUserResponse : ResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
        public bool IsDeleted { get; set; }
    }
}
