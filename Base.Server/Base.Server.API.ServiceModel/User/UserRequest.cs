﻿using ServiceStack;
using ServiceStack.ServiceHost;
using Base.Server.API.DTO;
using Base.Server.API.ServiceModel.Customer;

namespace Base.Server.API.ServiceModel.User
{

    [Route("/User/GetActiveUsers", "GET")]
    public class GetActiveUsers : IReturn<GetActiveUsersResponse>
    {
    }

    [Route("/User/GetUserByUserId", "GET")]
    public class GetUserByUserId : IReturn<GetUserByUserIdResponse>
    {
        public long UserId { get; set; }
    }

    [Route("/User/LoginUser", "GET")]
    public class LoginUser : IReturn<LoginUserResponse>
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }

    [Route("/User/CreateUser", "POST")]
    public class CreateUser : IReturn<CreateUserResponse>
    {
        public UserDTO User { get; set; }
    }

    [Route("/User/UpdateUser", "POST")]
    public class UpdateUser : IReturn<UpdateUserResponse>
    {
        public UserDTO User { get; set; }
    }

    [Route("/User/DeleteUser", "POST")]
    public class DeleteUser : IReturn<DeleteUserResponse>
    {
        public long UserId { get; set; }
        public string UpdatedBy { get; set; }
    }
}
