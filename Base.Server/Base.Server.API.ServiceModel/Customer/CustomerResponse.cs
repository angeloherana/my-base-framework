﻿using System.Collections.Generic;
using ServiceStack;
using ServiceStack.ServiceInterface.ServiceModel;
using Base.Server.API.DTO;

namespace Base.Server.API.ServiceModel.Customer
{
    public class GetActiveCustomersResponse : ResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
        public IEnumerable<CustomerDTO> Customers { get; set; }
    }
    public class GetCustomerByCustomerIdResponse
    {
        public ResponseStatus ResponseStatus { get; set; }
        public CustomerDTO Customer { get; set; }
    }
    public class CreateCustomerResponse : ResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
        public CustomerDTO Customer { get; set; }
    }
    public class UpdateCustomerResponse : ResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
        public CustomerDTO Customer { get; set; }
    }
    public class DeleteCustomerResponse : ResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
        public bool IsDeleted { get; set; }
    }
}
