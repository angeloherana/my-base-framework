﻿using ServiceStack;
using ServiceStack.ServiceHost;
using Base.Server.API.DTO;

namespace Base.Server.API.ServiceModel.Customer
{

    [Route("/Customer/GetActiveCustomers", "GET")]
    public class GetActiveCustomers : IReturn<GetActiveCustomersResponse>
    {
    }

    [Route("/Customer/GetCustomerByCustomerId", "GET")]
    public class GetCustomerByCustomerId : IReturn<GetCustomerByCustomerIdResponse>
    {
        public long CustomerId { get; set; }
    }

    [Route("/Customer/CreateCustomer", "POST")]
    public class CreateCustomer : IReturn<CreateCustomerResponse>
    {
        public CustomerDTO Customer { get; set; }
    }

    [Route("/Customer/UpdateCustomer", "POST")]
    public class UpdateCustomer : IReturn<UpdateCustomerResponse>
    {
        public CustomerDTO Customer { get; set; }
    }

    [Route("/Customer/DeleteCustomer", "POST")]
    public class DeleteCustomer : IReturn<DeleteCustomerResponse>
    {
        public long CustomerId { get; set; }
        public string UpdatedBy { get; set; }
    }
}
