﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.ServiceInterface.ServiceModel;
using Base.Server.API.DTO;

namespace Base.Server.API.ServiceModel.Menu
{
    public class GetParentMenuByGroupResponse : ResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
        public IEnumerable<MenuDTO> Menus { get; set; } 
    }

    public class GetChildMenuByGroupResponse : ResponseStatus
    {
        public ResponseStatus ResponseStatus { get; set; }
        public IEnumerable<MenuDTO> Menus { get; set; } 
    }
}
