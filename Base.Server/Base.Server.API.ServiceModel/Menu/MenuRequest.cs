﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.ServiceHost;
using Base.Server.API.DTO;
using Base.Server.API.ServiceModel.Menu;

namespace Base.Server.API.ServiceModel.Menu
{

    [Route("/Group/GetParentMenuByGroup", "GET")]
    public class GetParentMenuByGroup : IReturn<GetParentMenuByGroupResponse>
    {
        public IEnumerable<UserGroupDTO> UserGroups { get; set; }
    }

    [Route("/Group/GetChildMenuByGroup", "GET")]
    public class GetChildMenuByGroup : IReturn<GetChildMenuByGroupResponse>
    {
        public IEnumerable<UserGroupDTO> UserGroups { get; set; }
        public long ParentMenuId { get; set; }
    }
}
