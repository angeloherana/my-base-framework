﻿using System.Collections.Generic;
using System.Configuration;
using AutoMapper;
using NUnit.Framework;
using ServiceStack.ServiceClient.Web;
using Base.Server.API.DTO;
using Base.Server.API.ServiceModel.Menu;
using Base.Server.API.ServiceModel.User;
using Base.Server.Models;

namespace Base.Server.API.IntegrationTests
{
    [TestFixture]
    public class MenuAPITests
    {
        private readonly string _apiUrl = ConfigurationManager.AppSettings["API_URL"];
        private readonly string _apiKey = ConfigurationManager.AppSettings["ApiKey"];

        [Test]
        public void GetParentMenu_Test()
        {
            var client = new JsonServiceClient(_apiUrl)
            {
                LocalHttpWebRequestFilter =
                    req => req.Headers.Add("X-ApiKey", _apiKey),
            };

            Mapper.CreateMap<Menu, MenuDTO>();
            Mapper.CreateMap<MenuDTO, Menu>();
            Mapper.CreateMap<MenuGroup, MenuGroupDTO>();
            Mapper.CreateMap<MenuGroupDTO, MenuGroup>();

            var response1 = client.Get(new GetUserByUserId()
            {
                UserId = 3
            });

            var response2 = client.Get(new GetParentMenuByGroup()
            {
                UserGroups = Mapper.Map<IList<UserGroupDTO>>(response1.User.UserGroups)
            });


            foreach (var parentMenu in response2.Menus)
            {
                Assert.IsNull(parentMenu.ParentMenuId);
            }
        }

        [Test]
        public void GetChildMenu_Test()
        {
            var client = new JsonServiceClient(_apiUrl)
            {
                LocalHttpWebRequestFilter =
                    req => req.Headers.Add("X-ApiKey", _apiKey),
            };

            Mapper.CreateMap<Menu, MenuDTO>();
            Mapper.CreateMap<MenuDTO, Menu>();
            Mapper.CreateMap<MenuGroup, MenuGroupDTO>();
            Mapper.CreateMap<MenuGroupDTO, MenuGroup>();

            var response1 = client.Get(new GetUserByUserId()
            {
                UserId = 3
            });

            var response2 = client.Get(new GetChildMenuByGroup()
            {
                UserGroups = Mapper.Map<IList<UserGroupDTO>>(response1.User.UserGroups),
                ParentMenuId = 1
            });


            foreach (var childMenu in response2.Menus)
            {
                Assert.IsNotNull(childMenu.ParentMenuId);
            }
        }
    }
}
