﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.Server.API.DTO;
using Base.Server.API.ServiceModel.AppKey;
using NUnit.Framework;
using ServiceStack.ServiceClient.Web;
using Base.Server.Infrastructure.Services;

namespace Base.Server.API.IntegrationTests
{
    [TestFixture]
    public class AppKeyAPITests
    {
        private readonly string ApiUrl = ConfigurationManager.AppSettings["API_URL"];
        private readonly string ApiKey = ConfigurationManager.AppSettings["ApiKey"];

        [Test]
        public void GetAllAppKey_Test()
        {
            var client = new  JsonServiceClient(ApiUrl)
            {
                LocalHttpWebRequestFilter =
                    req => req.Headers.Add("X-ApiKey", ApiKey),
            };

            var response = client.Get(new GetAllAppKey());

            Assert.IsTrue(response.AppKeys.Any());
        }

        [Test]
        public void CreateAppKey_Test()
        {
            var client = new JsonServiceClient(ApiUrl)
            {
                LocalHttpWebRequestFilter =
                    req => req.Headers.Add("X-ApiKey", ApiKey),
            };
            var appKey = new AppKeyDTO
            {
                Name = "App Key Test1",
                ApiKey = Guid.NewGuid().ToString()
            };

            var response = client.Post(new CreateAppKey()
            {
                AppKey = appKey
            });

            Assert.IsNotNull(response);
        }
    }
}
