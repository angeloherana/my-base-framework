﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.Server.Infrastructure.Services;
using Base.Server.Infrastructure.Services.Interfaces;
using Base.Server.Services.Interfaces;
using Base.Server.Models;
using Base.Server.Repositories.UnitOfWork;

namespace Base.Server.Services
{
    public class UserService : IUserService
    {
        private readonly IBaseUow _baseUow;
        readonly IEncryptService _encryptService = new EncryptService();
        public UserService(IBaseUow baseUow)
        {
            _baseUow = baseUow;
        }

        public IEnumerable<User> GetActiveUsers()
        {
            return _baseUow.UserRepository.GetActiveUsers().ToList();
        }

        public User GetUserByUserId(long userId)
        {
            return _baseUow.UserRepository.GetUserByUserId(userId);
        }

        public User LoginUser(string userName, string password)
        {
            var encryptedPass = _encryptService.Encrypt(password);
            var result = _baseUow.UserRepository.GetUserByUserNamePassword(userName, encryptedPass);
            if (result == null)
            {
                throw new UnauthorizedAccessException("Wrong Username and Password combination");
            }
            if (!result.IsActive)
            {
                throw new UnauthorizedAccessException("User is not active.");
            }
            return result;
        }

        public User CreateUser(User user)
        {
            var newUser = user;
            newUser.CreatedDate = DateTime.Now;
            newUser.Password = _encryptService.Encrypt(newUser.Password);

            _baseUow.UserRepository.Add(newUser);
            _baseUow.Commit();

            return newUser;
        }

        public User UpdateUser(User user)
        {
            var existingUser = user;

            existingUser.UpdatedDate = DateTime.Now;
            _baseUow.UserRepository.Update(existingUser);
            _baseUow.Commit();

            return existingUser;
        }

        public bool DeleteUser(long userId, string updatedBy)
        {
            var existingUser = _baseUow.UserRepository.GetUserByUserId(userId);

            existingUser.UpdatedBy = updatedBy;
            existingUser.UpdatedDate = DateTime.Now;
            existingUser.IsActive = false;

            _baseUow.UserRepository.Update(existingUser);
            _baseUow.Commit();
            return true;
        }
    }
}
