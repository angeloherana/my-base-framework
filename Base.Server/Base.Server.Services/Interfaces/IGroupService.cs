﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.Server.Models;

namespace Base.Server.Services.Interfaces
{
    public interface IGroupService
    {
        IEnumerable<Group> GetActiveGroups();
        Group GetGroupByGroupId(long groupId);
        Group CreateGroup(Group group);
        Group UpdateGroup(Group group);
        bool DeleteGroup(long groupId, string updatedBy);
    }
}
