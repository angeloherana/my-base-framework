﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.Server.Models;

namespace Base.Server.Services.Interfaces
{
    public interface IUserGroupService
    {
        IEnumerable<UserGroup> GetAllUserGroups();
        UserGroup CreateUserGroup(UserGroup userGroup);
        bool DeleteUserGroup(long userGroupId);
    }
}
