﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.Server.Models;

namespace Base.Server.Services.Interfaces
{
    public interface ICustomerService
    {
        IEnumerable<Customer> GetActiveCustomers();
        Customer GetCustomerByCustomerId(long customerId);
        Customer CreateCustomer(Customer customer);
        Customer UpdateCustomer(Customer customer);
        bool DeleteCustomer(long customerId, string updatedBy);
    }
}
