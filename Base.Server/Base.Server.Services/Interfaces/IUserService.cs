﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.Server.Models;

namespace Base.Server.Services.Interfaces
{
    public interface IUserService
    {
        User GetUserByUserId(long userId);
        User LoginUser(string userName, string password);
        IEnumerable<User> GetActiveUsers();
        User CreateUser(User user);
        User UpdateUser(User user);
        bool DeleteUser(long userId, string updatedBy);
    }
}
