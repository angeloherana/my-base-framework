﻿using System.Collections.Generic;
using Base.Server.Models;

namespace Base.Server.Services.Interfaces
{
    public interface IAppKeyService
    {
        IEnumerable<AppKey> GetAllAppKey();
        AppKey CreateAppKey(AppKey appKey);
        bool CheckIfApiKeyIsValid(string apiKey);
    }
}
