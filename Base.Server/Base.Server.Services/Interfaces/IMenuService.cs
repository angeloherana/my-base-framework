﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.Server.Models;

namespace Base.Server.Services.Interfaces
{
    public interface IMenuService
    {
        IEnumerable<Menu> GetParentMenuByGroup(IList<UserGroup> userGroups);
        IEnumerable<Menu> GetChildMenuByGroup(IList<UserGroup> userGroups, long parentMenuId);
    }
}
