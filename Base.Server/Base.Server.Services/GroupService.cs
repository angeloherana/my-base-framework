﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.Server.Services.Interfaces;
using Base.Server.Models;
using Base.Server.Repositories.UnitOfWork;

namespace Base.Server.Services
{
    public class GroupService : IGroupService
    {
        private readonly IBaseUow _baseUow;
        public GroupService(IBaseUow baseUow)
        {
            _baseUow = baseUow;
        }

        public IEnumerable<Group> GetActiveGroups()
        {
            return _baseUow.GroupRepository.GetActiveGroups().ToList();
        }

        public Group GetGroupByGroupId(long groupId)
        {
            return _baseUow.GroupRepository.GetById(groupId);
        }

        public Group CreateGroup(Group group)
        {
            var newGroup = group;
            newGroup.CreatedDate = DateTime.Now;

            _baseUow.GroupRepository.Add(newGroup);
            _baseUow.Commit();

            return newGroup;
        }

        public Group UpdateGroup(Group group)
        {
            var existingGroup = group;
            existingGroup.UpdatedDate = DateTime.Now;

            _baseUow.GroupRepository.Update(existingGroup);
            _baseUow.Commit();

            return existingGroup;
        }

        public bool DeleteGroup(long groupId, string updatedBy)
        {
            var existingGroup = _baseUow.GroupRepository.GetById(groupId);

            existingGroup.UpdatedBy = updatedBy;
            existingGroup.UpdatedDate = DateTime.Now;
            existingGroup.IsActive = false;

            _baseUow.GroupRepository.Update(existingGroup);
            _baseUow.Commit();
            return true;
        }
    }
}
