﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.Server.Services.Interfaces;
using Base.Server.Models;
using Base.Server.Repositories.UnitOfWork;

namespace Base.Server.Services
{
    public class UserGroupService : IUserGroupService
    {
        private readonly IBaseUow _baseUow;
        public UserGroupService(IBaseUow baseUow)
        {
            _baseUow = baseUow;
        }

        public IEnumerable<UserGroup> GetAllUserGroups()
        {
            return _baseUow.UserGroupRepository.GetAll().ToList();
        }

        public UserGroup CreateUserGroup(UserGroup userGroup)
        {
            var newUserGroup = userGroup;
            newUserGroup.CreatedDate = DateTime.Now;

            _baseUow.UserGroupRepository.Add(newUserGroup);
            _baseUow.Commit();

            return newUserGroup;
        }

        public bool DeleteUserGroup(long userGroupId)
        {
            _baseUow.UserGroupRepository.Delete(userGroupId);
            _baseUow.Commit();
            return true;
        }
    }
}
