﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.Server.Models;
using Base.Server.Repositories.UnitOfWork;
using Base.Server.Services.Interfaces;

namespace Base.Server.Services
{
    public class MenuService : IMenuService
    {
        private readonly IBaseUow _baseUow;
        public MenuService(IBaseUow baseUow)
        {
            _baseUow = baseUow;
        }

        public IEnumerable<Menu> GetParentMenuByGroup(IList<UserGroup> userGroups)
        {
            var groups = userGroups;
            var allowedMenu = new List<Menu>();
            var items = _baseUow.MenuRepository.GetParentMenus().ToList();

            foreach (var item in items)
            {
                allowedMenu.AddRange(from menuGroup in item.MenuGroups
                                     where groups.Select(g => g.GroupId).Contains(menuGroup.GroupId)
                                     select item);
            }
            return allowedMenu.Distinct().ToList();
        }

        public IEnumerable<Menu> GetChildMenuByGroup(IList<UserGroup> userGroups, long parentMenuId)
        {
            var groups = userGroups;
            var allowedMenu = new List<Menu>();
            var items = _baseUow.MenuRepository.GetChildMenus(parentMenuId).ToList();

            foreach (var item in items)
            {
                allowedMenu.AddRange(from menuGroup in item.MenuGroups
                                     where groups.Select(g => g.GroupId).Contains(menuGroup.GroupId)
                                     select item);
            }
            return allowedMenu.Distinct().ToList();
        }
    }
}
