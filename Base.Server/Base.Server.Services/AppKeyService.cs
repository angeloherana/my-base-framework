﻿using System;
using System.Collections.Generic;
using System.Linq;
using Base.Server.Infrastructure.Services;
using Base.Server.Infrastructure.Services.Interfaces;
using Base.Server.Services.Interfaces;
using Base.Server.Models;
using Base.Server.Repositories.UnitOfWork;

namespace Base.Server.Services
{
    public class AppKeyService : IAppKeyService
    {
        private readonly IBaseUow _baseUow;
        readonly IEncryptService _encryptService = new EncryptService();


        public AppKeyService(IBaseUow baseUow)
        {
            _baseUow = baseUow;
        }

        public IEnumerable<AppKey> GetAllAppKey()
        {
            return _baseUow.AppKeyRepository.GetAll().ToList();
        }

        public AppKey CreateAppKey(AppKey appKey)
        {
            appKey.ApiKey = _encryptService.Encrypt(appKey.ApiKey);
            _baseUow.AppKeyRepository.Add(appKey);
            _baseUow.Commit();
            return appKey;
        }
        public bool CheckIfApiKeyIsValid(string apiKey)
        {
            var encryptedApiKey = _encryptService.Encrypt(apiKey) ?? string.Empty;
            var result = _baseUow.AppKeyRepository.GetAppKeyByApiKey(encryptedApiKey);
            return result != null;
        }
    }
}
