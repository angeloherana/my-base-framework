﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.Server.Services.Interfaces;
using Base.Server.Models;
using Base.Server.Repositories.UnitOfWork;

namespace Base.Server.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly IBaseUow _baseUow;
        public CustomerService(IBaseUow baseUow)
        {
            _baseUow = baseUow;
        }

        public IEnumerable<Customer> GetActiveCustomers()
        {
            return _baseUow.CustomerRepository.GetActiveCustomers().ToList();
        }

        public Customer GetCustomerByCustomerId(long customerId)
        {
            return _baseUow.CustomerRepository.GetById(customerId);
        }

        public Customer CreateCustomer(Customer customer)
        {
            var newCustomer = customer;
            newCustomer.CreatedDate = DateTime.Now;

            _baseUow.CustomerRepository.Add(newCustomer);
            _baseUow.Commit();

            return newCustomer;
        }

        public Customer UpdateCustomer(Customer customer)
        {
            var existingCustomer = customer;
            existingCustomer.UpdatedDate = DateTime.Now;

            _baseUow.CustomerRepository.Update(existingCustomer);
            _baseUow.Commit();

            return existingCustomer;
        }

        public bool DeleteCustomer(long customerId, string updatedBy)
        {
            var existingCustomer = _baseUow.CustomerRepository.GetById(customerId);

            existingCustomer.UpdatedBy = updatedBy;
            existingCustomer.UpdatedDate = DateTime.Now;
            existingCustomer.IsActive = false;

            _baseUow.CustomerRepository.Update(existingCustomer);
            _baseUow.Commit();
            return true;
        }
    }
}
