﻿using System.Collections;
using System.Collections.Generic;
using AutoMapper;
using ServiceStack;
using ServiceStack.ServiceInterface;
using Base.Server.API.DTO;
using Base.Server.API.ServiceModel.Customer;
using Base.Server.API.Services.Interfaces;
using Base.Server.Models;
using Base.Server.Services.Interfaces;

namespace Base.Server.API.Services
{
    public class CustomerAPIService : Service, ICustomerAPIService
    {
        private readonly ICustomerService _customerService;

        public CustomerAPIService(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        public GetActiveCustomersResponse Get(GetActiveCustomers request)
        {
            var response = _customerService.GetActiveCustomers();

            return new GetActiveCustomersResponse()
            {
                Customers = Mapper.Map<IEnumerable<CustomerDTO>>(response)
            };
        }

        public GetCustomerByCustomerIdResponse Get(GetCustomerByCustomerId request)
        {
            var response = _customerService.GetCustomerByCustomerId(request.CustomerId);

            return new GetCustomerByCustomerIdResponse()
            {
                Customer = Mapper.Map<CustomerDTO>(response)
            };
        }

        public CreateCustomerResponse Post(CreateCustomer request)
        {
            var response = _customerService.CreateCustomer(Mapper.Map<Customer>(request.Customer));

            return new CreateCustomerResponse()
            {
                Customer = Mapper.Map<CustomerDTO>(response)
            };
        }

        public UpdateCustomerResponse Post(UpdateCustomer request)
        {
            var response = _customerService.UpdateCustomer(Mapper.Map<Customer>(request.Customer));

            return new UpdateCustomerResponse()
            {
                Customer = Mapper.Map<CustomerDTO>(response)
            };
        }

        public DeleteCustomerResponse Post(DeleteCustomer request)
        {
            var isDeleted = _customerService.DeleteCustomer(request.CustomerId, request.UpdatedBy);

            return new DeleteCustomerResponse()
            {
                IsDeleted = isDeleted
            };
        }
    }
}
