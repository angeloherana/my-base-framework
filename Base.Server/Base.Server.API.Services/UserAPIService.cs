﻿using System.Collections;
using System.Collections.Generic;
using AutoMapper;
using ServiceStack;
using ServiceStack.ServiceInterface;
using Base.Server.API.DTO;
using Base.Server.API.ServiceModel.User;
using Base.Server.API.Services.Interfaces;
using Base.Server.Models;
using Base.Server.Services.Interfaces;

namespace Base.Server.API.Services
{
    public class UserAPIService : Service, IUserAPIService
    {
        private readonly IUserService _userService;

        public UserAPIService(IUserService userService)
        {
            _userService = userService;
        }

        public GetActiveUsersResponse Get(GetActiveUsers request)
        {
            var response = _userService.GetActiveUsers();

            return new GetActiveUsersResponse()
            {
                Users = Mapper.Map<IEnumerable<UserDTO>>(response)
            };
        }

        public GetUserByUserIdResponse Get(GetUserByUserId request)
        {
            var response = _userService.GetUserByUserId(request.UserId);

            return new GetUserByUserIdResponse()
            {
                User = Mapper.Map<UserDTO>(response)
            };
        }

        public LoginUserResponse Get(LoginUser request)
        {
            var response = _userService.LoginUser(request.UserName, request.Password);

            return new LoginUserResponse()
            {
                User = Mapper.Map<UserDTO>(response)
            };
        }

        public CreateUserResponse Post(CreateUser request)
        {
            var response = _userService.CreateUser(Mapper.Map<User>(request.User));

            return new CreateUserResponse()
            {
                User = Mapper.Map<UserDTO>(response)
            };
        }

        public UpdateUserResponse Post(UpdateUser request)
        {
            var response = _userService.UpdateUser(Mapper.Map<User>(request.User));

            return new UpdateUserResponse()
            {
                User = Mapper.Map<UserDTO>(response)
            };
        }

        public DeleteUserResponse Post(DeleteUser request)
        {
            var isDeleted = _userService.DeleteUser(request.UserId, request.UpdatedBy);

            return new DeleteUserResponse()
            {
                IsDeleted = isDeleted
            };
        }
    }
}
