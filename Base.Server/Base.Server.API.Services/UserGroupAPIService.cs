﻿using System.Collections;
using System.Collections.Generic;
using AutoMapper;
using ServiceStack;
using ServiceStack.ServiceInterface;
using Base.Server.API.DTO;
using Base.Server.API.ServiceModel.User;
using Base.Server.API.ServiceModel.UserGroup;
using Base.Server.API.Services.Interfaces;
using Base.Server.Models;
using Base.Server.Services.Interfaces;

namespace Base.Server.API.Services
{
    public class UserGroupAPIService : Service, IUserGroupAPIService
    {
        private readonly IUserGroupService _userGroupService;

        public UserGroupAPIService(IUserGroupService userGroupService)
        {
            _userGroupService = userGroupService;
        }

        public GetAllUserGroupsResponse Get(GetAllUserGroups request)
        {
            var response = _userGroupService.GetAllUserGroups();

            return new GetAllUserGroupsResponse()
            {
                UserGroups = Mapper.Map<IEnumerable<UserGroupDTO>>(response)
            };
        }

        public CreateUserGroupResponse Post(CreateUserGroup request)
        {
            var response = _userGroupService.CreateUserGroup(Mapper.Map<UserGroup>(request.UserGroup));

            return new CreateUserGroupResponse()
            {
                UserGroup = Mapper.Map<UserGroupDTO>(response)
            };
        }

        public DeleteUserGroupResponse Post(DeleteUserGroup request)
        {
            var isDeleted = _userGroupService.DeleteUserGroup(request.UserGroupId);

            return new DeleteUserGroupResponse()
            {
                IsDeleted = isDeleted
            };
        }
    }
}
