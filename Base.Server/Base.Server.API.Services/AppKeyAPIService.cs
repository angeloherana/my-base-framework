﻿using System.Collections;
using System.Collections.Generic;
using AutoMapper;
using ServiceStack;
using ServiceStack.ServiceInterface;
using Base.Server.API.DTO;
using Base.Server.API.ServiceModel.AppKey;
using Base.Server.API.Services.Interfaces;
using Base.Server.Models;
using Base.Server.Services.Interfaces;

namespace Base.Server.API.Services
{
    public class AppKeyAPIService : Service, IAppKeyAPIService
    {
        private readonly IAppKeyService _appKeyService;

        public AppKeyAPIService(IAppKeyService appKeyService)
        {
            _appKeyService = appKeyService;
        }

        public CreateAppKeyResponse Post(CreateAppKey request)
        {
            var response = _appKeyService.CreateAppKey(Mapper.Map<AppKey>(request.AppKey));

            return new CreateAppKeyResponse()
            {
                AppKey = Mapper.Map<AppKeyDTO>(response)
            };
        }

        public GetAllAppKeyResponse Get(GetAllAppKey request)
        {
            var response = _appKeyService.GetAllAppKey();

            return new GetAllAppKeyResponse()
            {
                AppKeys = Mapper.Map<IEnumerable<AppKeyDTO>>(response)
            };
        }
    }
}
