﻿using System.Collections;
using System.Collections.Generic;
using AutoMapper;
using ServiceStack;
using ServiceStack.ServiceInterface;
using Base.Server.API.DTO;
using Base.Server.API.ServiceModel.Group;
using Base.Server.API.Services.Interfaces;
using Base.Server.Models;
using Base.Server.Services.Interfaces;

namespace Base.Server.API.Services
{
    public class GroupAPIService : Service, IGroupAPIService
    {
        private readonly IGroupService _groupService;

        public GroupAPIService(IGroupService groupService)
        {
            _groupService = groupService;
        }

        public GetActiveGroupsResponse Get(GetActiveGroups request)
        {
            var response = _groupService.GetActiveGroups();

            return new GetActiveGroupsResponse()
            {
                Groups = Mapper.Map<IEnumerable<GroupDTO>>(response)
            };
        }

        public GetGroupByGroupIdResponse Get(GetGroupByGroupId request)
        {
            var response = _groupService.GetGroupByGroupId(request.GroupId);

            return new GetGroupByGroupIdResponse()
            {
                Group = Mapper.Map<GroupDTO>(response)
            };
        }

        public CreateGroupResponse Post(CreateGroup request)
        {
            var response = _groupService.CreateGroup(Mapper.Map<Group>(request.Group));

            return new CreateGroupResponse()
            {
                Group = Mapper.Map<GroupDTO>(response)
            };
        }

        public UpdateGroupResponse Post(UpdateGroup request)
        {
            var response = _groupService.UpdateGroup(Mapper.Map<Group>(request.Group));

            return new UpdateGroupResponse()
            {
                Group = Mapper.Map<GroupDTO>(response)
            };
        }

        public DeleteGroupResponse Post(DeleteGroup request)
        {
            var isDeleted = _groupService.DeleteGroup(request.GroupId, request.UpdatedBy);

            return new DeleteGroupResponse()
            {
                IsDeleted = isDeleted
            };
        }
    }
}
