﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using ServiceStack.ServiceInterface;
using Base.Server.API.DTO;
using Base.Server.API.ServiceModel.Menu;
using Base.Server.API.Services.Interfaces;
using Base.Server.Models;
using Base.Server.Services.Interfaces;

namespace Base.Server.API.Services
{
    public class MenuAPIService : Service, IMenuAPIService
    {
        private readonly IMenuService _menuService;

        public MenuAPIService(IMenuService menuService)
        {
            _menuService = menuService;
        }

        public GetParentMenuByGroupResponse Get(GetParentMenuByGroup request)
        {
            var response = _menuService.GetParentMenuByGroup(Mapper.Map<IList<UserGroup>>(request.UserGroups));

            return new GetParentMenuByGroupResponse()
            {
                Menus = Mapper.Map<IEnumerable<MenuDTO>>(response)
            };
        }

        public GetChildMenuByGroupResponse Get(GetChildMenuByGroup request)
        {
            var response = _menuService.GetChildMenuByGroup(Mapper.Map<IList<UserGroup>>(request.UserGroups), request.ParentMenuId);

            return new GetChildMenuByGroupResponse()
            {
                Menus = Mapper.Map<IEnumerable<MenuDTO>>(response)
            };
        }

    }
}
