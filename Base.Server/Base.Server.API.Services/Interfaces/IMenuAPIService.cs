﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.Server.API.ServiceModel.Menu;

namespace Base.Server.API.Services.Interfaces
{
    public interface IMenuAPIService
    {
        GetParentMenuByGroupResponse Get(GetParentMenuByGroup request);
        GetChildMenuByGroupResponse Get(GetChildMenuByGroup request);
    }
}
