﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.Server.API.ServiceModel.Group;

namespace Base.Server.API.Services.Interfaces
{
    public interface IGroupAPIService
    {
        GetActiveGroupsResponse Get(GetActiveGroups request);
        GetGroupByGroupIdResponse Get(GetGroupByGroupId request);
        CreateGroupResponse Post(CreateGroup request);
        UpdateGroupResponse Post(UpdateGroup request);
        DeleteGroupResponse Post(DeleteGroup request);
    }
}
