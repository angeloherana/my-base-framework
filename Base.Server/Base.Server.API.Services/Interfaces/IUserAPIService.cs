﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.Server.API.ServiceModel.User;

namespace Base.Server.API.Services.Interfaces
{
    public interface IUserAPIService
    {
        GetActiveUsersResponse Get(GetActiveUsers request);
        GetUserByUserIdResponse Get(GetUserByUserId request);
        LoginUserResponse Get(LoginUser request);
        CreateUserResponse Post(CreateUser request);
        UpdateUserResponse Post(UpdateUser request);
        DeleteUserResponse Post(DeleteUser request);
    }
}
