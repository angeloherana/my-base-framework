﻿using Base.Server.API.ServiceModel.AppKey;

namespace Base.Server.API.Services.Interfaces
{
    public interface IAppKeyAPIService
    {
        CreateAppKeyResponse Post(CreateAppKey request);
        GetAllAppKeyResponse Get(GetAllAppKey request);
    }
}
