﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.Server.API.ServiceModel.UserGroup;

namespace Base.Server.API.Services.Interfaces
{

    public interface IUserGroupAPIService
    {
        GetAllUserGroupsResponse Get(GetAllUserGroups request);
        CreateUserGroupResponse Post(CreateUserGroup request);
        DeleteUserGroupResponse Post(DeleteUserGroup request);
    }
}
