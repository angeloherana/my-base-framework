﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.Server.API.ServiceModel.Customer;

namespace Base.Server.API.Services.Interfaces
{

    public interface ICustomerAPIService
    {
        GetActiveCustomersResponse Get(GetActiveCustomers request);
        GetCustomerByCustomerIdResponse Get(GetCustomerByCustomerId request);
        CreateCustomerResponse Post(CreateCustomer request);
        UpdateCustomerResponse Post(UpdateCustomer request);
        DeleteCustomerResponse Post(DeleteCustomer request);
    }
}
