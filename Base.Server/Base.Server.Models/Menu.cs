﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base.Server.Models
{
    public class Menu
    {
        public Menu()
        {
            MenuGroups = new List<MenuGroup>();
        }
        public long MenuId { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public long? ParentMenuId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public ICollection<MenuGroup> MenuGroups { get; set; }
    }
}
