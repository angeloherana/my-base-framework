﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base.Server.Models
{
    public class AppKey
    {
        [Key]
        public long AppKeyId { get; set; }
        public string Name { get; set; }
        public string ApiKey { get; set; }
    }
}
