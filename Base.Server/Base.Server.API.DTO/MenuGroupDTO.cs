﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base.Server.API.DTO
{
    public class MenuGroupDTO
    {
        public long MenuGroupId { get; set; }
        public long MenuId { get; set; }
        public long GroupId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
