﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base.Server.API.DTO
{
    public class UserGroupDTO
    {
        public long UserGroupId { get; set; }
        public long UserId { get; set; }
        public long GroupId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
